function [] = Fig_2_C(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    r1_t=R1_C./R0_C;
    r2_t=R2_C./R0_C;

    r1=max(r1_t,r2_t);
    r2=min(r1_t,r2_t);

    plot(r1,r2,'.')
    hold on
    plot_grids();
    pbaspect([1 1 1])

end

