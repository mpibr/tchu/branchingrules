function [] = Fig_2_F(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    rr_o=[OTO.R1./OTO.R0;OTO.R2./OTO.R0];
    rr_a=[ALI.R1./ALI.R0,ALI.R2./ALI.R0];
    rr_c=[R1_C./R0_C,R2_C./R0_C];

    
    
    
    rr=0:0.1:1.5;


    [yC,xC]=hist(rr_c,rr);
    hold on
    [yA,xA]=hist(rr_a,rr);
    [yO,xO]=hist(rr_o,rr);

    plot(xC,yC./sum(yC),'k');
    plot(xA,yA./sum(yA),'r');
    plot(xO,yO./sum(yO),'b');


    plot(median(rr_c),0,'ok');
    plot(median(rr_a),0,'or');
    plot(median(rr_o),0,'ob');
    

end

