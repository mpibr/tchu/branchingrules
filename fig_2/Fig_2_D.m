function [] = Fig_2_D(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    r1_t=ALI.R1;
    r2_t=ALI.R2;

    r1=max(r1_t,r2_t);
    r2=min(r1_t,r2_t);

    plot(r1,r2,'.')
    hold on
    plot_grids();
    pbaspect([1 1 1])




end

