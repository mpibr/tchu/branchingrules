function [] = plot_grids(i)
    

    XB=[0 0 1];
    YB_T=[0 1 0];
    YB_B=[0 0 0];


    x01=0:0.01:1;
    x015=[0 1 1 1.5];

    yb_b=0.*x01;
    yb_t=1-x01;

    yg_b=yb_t;
    yg_t=(1-x01.^2).^0.5;

    yr_b=yg_t;
    yr_t=ones(1,length(yr_b));

    yc_b=[1 1 0 0];
    yc_t=[1.5 1.5 1.5 1.5];





    plot([XB fliplr(XB)],[YB_B fliplr(YB_T)],'b');
    hold on
    plot([x01 fliplr(x01)],[yg_b fliplr(yg_t)],'g');
    plot([x01 fliplr(x01)],[yr_b fliplr(yr_t)],'r');
    plot([x015 fliplr(x015)],[yc_b fliplr(yc_t)],'c');
    plot(x01,x01)
    x7=[0 1.5];
    plot(x7,x7)
    axis([0 1.5 0 1.5])
end

