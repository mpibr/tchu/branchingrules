function [ RP ] = bisec( R1,R2 )
    m_original=-1000;
    M_original=100;

    for i=1:length(R1)
        r1=R1(i);
        r2=R2(i);
        i
        if(max(r1,r2)<1)
            m=0;
            M=M_original;

            ERR=0.0001;
            a=1/2*(m+M);
            
            E=1-r1^a-r2^a;
            while((abs(E)>ERR)||(isnan(E)==1))
                if(E>0)
                    M=a;
                    a=1/2*(m+M);
                end
                if(E<0)
                    m=a;
                    a=1/2*(m+M);
                end
                E=1-r1^a-r2^a;
            end
        elseif(min(r1,r2)>1)
            m=m_original;
            M=0;

            ERR=0.0001;
            a=1/2*(m+M);
            
            E=1-r1^a-r2^a;
            while((abs(E)>ERR)||(isnan(E)==1))
                if(E>0)
                    m=a;
                    a=1/2*(m+M);
                end
                if(E<0)
                    M=a;
                    a=1/2*(m+M);
                end
                E=1-r1^a-r2^a;
            end
        else
            a=NaN;
        end
        RP(i)=a;

    end
end

