function [] = Fig_2_G(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    RP_A=bisec(ALI.R1./ALI.R0,ALI.R2./ALI.R0);
    RP_O=bisec(OTO.R1./OTO.R0,OTO.R2./OTO.R0);
    RP_C=bisec(R1_C./R0_C,R2_C./R0_C);

    disp('________________________')
    disp('_________median_________')
    disp('________________________')

    YA=quantile(RP_A,[.25 .5 .75])
    YO=quantile(RP_O,[.25 .5 .75])
    YC=quantile(RP_C,[.25 .5 .75])

    disp('________________________')
    disp('__________mean__________')
    disp('________________________')
    [nanmean(RP_A) nanstd(RP_A)]
    [nanmean(RP_C) nanstd(RP_C)]
    [nanmean(RP_O) nanstd(RP_O)]


    d=.5;
    BINS=-2:d:7;

    [yO,x]=hist(RP_O,BINS);
    [yA,x]=hist(RP_A,BINS);
    [yC,x]=hist(RP_C,BINS);




    plot(x,yO./sum(yO),'b')
    hold on
    plot(x,yA./sum(yA),'r')
    plot(x,yC./sum(yC),'k')




    errorbar(YO(2),0.35,(YO(2)-YO(1)),YO(3)-YO(2),'horizontal','ob');
    errorbar(YA(2),0.3,(YA(2)-YA(1)),YA(3)-YA(2),'horizontal','or');
    errorbar(YC(2),0.25,(YC(2)-YC(1)),YC(3)-YC(2),'horizontal','ok');

end






%%

