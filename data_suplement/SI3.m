close all
clear all
clc

load('all_bio_data.mat')



LL1=ALI.L1(ALI.L1>0 & ALI.L1<1000);
LL2=ALI.L2(ALI.L1>0 & ALI.L1<1000);
ll1=max(LL1,LL2);
ll2=min(LL1,LL2);

lambda=110;
RR=[ALI.R1./ALI.R0 ALI.R2./ALI.R0];
R2=median(RR);
R1=0:0.0001:3;


Gamma=0.5:0.01:2;
Ropt_s=zeros(length(Gamma),1);
Ropt_c= zeros(length(Gamma),1);

size(Ropt_s)
size(Ropt_c)

for j=1:length(ll1)
    L1=ll1(j);
    L2=ll2(j);
    R_1S=RHO(R1,R2,1,1,1,L1,L2,lambda);
    R_2S=RHO(R1,R2,1,2,1,L1,L2,lambda);
    R_1C=RHO(R1,R2,2,1,1,L1,L2,lambda);
    R_2C=RHO(R1,R2,2,2,1,L1,L2,lambda);

    DNS=abs(R_1S-R_2S);
	Ropt_base_s=mean(R1(find(DNS==min(DNS))));

	DNC=abs(R_1C-R_2C);
	Ropt_base_c=mean(R1(find(DNC==min(DNC))));

    
    
    for i=1:length(Gamma)
        g=Gamma(i);
        
        DNS=abs(R_1S-g.*R_2S);
        TEMP_s=mean(R1(find(DNS==min(DNS))));
        Ropt_s(i)=Ropt_s(i)+TEMP_s/Ropt_base_s;

        DNC=abs(R_1C-g.*R_2C);
        TEMP_c=mean(R1(find(DNC==min(DNC))));
        Ropt_c(i)=Ropt_c(i)+TEMP_c/Ropt_base_c;

    end

end
Ropt_c=(Ropt_c./length(ll1)-1)*100;
Ropt_s=(Ropt_s./length(ll1)-1)*100;



plot(Gamma,Ropt_s,'b')
hold on
plot(Gamma,Ropt_c,'r')

