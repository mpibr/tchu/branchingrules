function [] = SI_1(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    if(exist('all_fluo.mat')==2)
        load('all_fluo.mat');
    else
        here=pwd;
        cd ('./../FLUO_DATA')
        IMPORT_FLUO(path_1);
        cd (here);
    end
    
    
    
    mm=[0 0 0 0 1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4];
    DD=[1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];

    for i=1:length(mm)
        GFP_R(i).gfp=RES(i).gfp_N(:,4);
        NLG_R(i).nlg=RES(i).ngl_N(:,4);
    end




    for i=1:length(mm)
        LLG(i)=length(GFP_R(i).gfp);
        LLN(i)=length(NLG_R(i).nlg);
    end

    indx=[];
    NMIN=17;

    for i=1:length(mm)
        if(LLG(i)>=NMIN & LLN(i)>=NMIN)
            indx=[indx,i];
        end
    end
    RS=(R1_C+R2_C)./R0_C;
    RC=(R1_C.^2+R2_C.^2)./R0_C.^2;





    for i=1:length(mm)
        MG(i)=mean(GFP_R(i).gfp);
        SG(i)=std(GFP_R(i).gfp);
        
        YG=quantile(GFP_R(i).gfp,[.25 .5 .75]);
        MDG(i,:)=YG;

        MN(i)=mean(NLG_R(i).nlg);
        SN(i)=std(NLG_R(i).nlg);
        YN=quantile(NLG_R(i).nlg,[.25 .5 .75]);
        MDN(i,:)=YN;

    end




YS=quantile(RS,[.25 .5 .75])
[YS(2) YS(2)-YS(1) YS(3)-YS(2)]
YC=quantile(RC,[.25 .5 .75])
[YC(2) YC(2)-YC(1) YC(3)-YC(2)]



ms=mean(RS);
ss=std(RS);
mc=mean(RC);
sc=std(RC);


YN=[ms ms];
YNp=[ms+ss ms+ss];
YNm=[ms-ss ms-ss];

YG=[mc mc];
YGp=[mc+sc mc+sc];
YGm=[mc-sc mc-sc];

x=[0 21];



subplot(2,1,1)
errorbar([1:length(indx)]',[MG(indx)],[SG(indx)],[SG(indx)],'.r')
title('mean mean')
hold on
plot(x,YG,'r')
plot(x,YGp,'r')
plot(x,YGm,'r')

errorbar([1:length(indx)]'+0.25,[MN(indx)],[SN(indx)],[SN(indx)],'.b')

plot(x,YN,'b--')
plot(x,YNp,'b--')
plot(x,YNm,'b--')

axis([0 length(indx)+1 0 4])



subplot(2,1,2)
errorbar([1:length(indx)]',[MDG(indx,2)],[MDG(indx,2)-MDG(indx,1)],[MDG(indx,3)-MDG(indx,2)],'.r')
hold on
errorbar([1:length(indx)]'+0.25,[MDN(indx,2)],[MDN(indx,2)-MDN(indx,1)],[MDN(indx,3)-MDN(indx,2)],'.b')
plot(x,YG,'r')
plot(x,YGp,'r')
plot(x,YGm,'r')
title('median median')
hold on
plot(x,YN,'b--')
plot(x,YNp,'b--')
plot(x,YNm,'b--')

axis([0 length(indx)+1 0 4])




    
end

