clear all
close all
clc


BASE.l=2;
BASE.r=0.05;
BASE.R=0.5;
BASE.R0=0.6;


[l, r, R, R0]=assign_from_base(BASE);
[rS_ref, rV_ref] = calc_spine(l, r, R, R0);



%%
figure(1)
subplot(4,2,1)

[l, r, R, R0]=assign_from_base(BASE);
R=0:0.01:1;  %spine radius
[rS, rV] = calc_spine(l, r, R, R0);

S=2.*pi.*r.*l+4.*pi.*R.^2;
V=pi.*r.^2.*l+3/4.*pi.*R.^3;

S0=R0*pi*2;
V0=R0^2*pi;
rS=S./(S+S0);
rV=V./(V+V0);
plot(R,rS,'b')
hold on
plot(R,rV,'r')
title('varying radius spine')
ylabel('fraction of protein in spine')
xlabel('radius of dendritic spine')


axis([0 max(R) 0 1])


subplot(4,2,2)
plot((R./BASE.R-1).*100,(rS./rS_ref-1).*100,'b')
hold on
plot((R./BASE.R-1).*100,(rV./rV_ref-1).*100,'r')
    axis([-10 +10 -25 25])

title('varying radius spine')
xlabel('radius of dendritic spine')
ylabel('% variation of protein in synapse')

%%
[l, r, R, R0]=assign_from_base(BASE);
r=0:0.01:0.1; %neck radius
[rS, rV] = calc_spine(l, r, R, R0);

figure(1)
subplot(4,2,3)
    plot(r,rS,'b')
    hold on
    plot(r,rV,'r')
title('varying radius neck')
xlabel('radius of spine neck [\mu m]')
ylabel('fraction of protein in spine')

axis([0 max(r) 0 1])

subplot(4,2,4)
plot((r./BASE.r-1).*100,(rS./rS_ref-1).*100,'b')
hold on
plot((r./BASE.r-1).*100,(rV./rV_ref-1).*100,'r')
    axis([-10 +10 -1 1])

title('varying radius neck')
xlabel('radius of spine neck [\mu m]')
ylabel('% variation of protein in synapse')

%%

subplot(4,2,5)

[l, r, R, R0]=assign_from_base(BASE);
R0=0:0.01:1.2;
[rS, rV] = calc_spine(l, r, R, R0);


S=2.*pi.*r.*l+4.*pi.*R.^2;
V=pi.*r.^2.*l+3/4.*pi.*R.^3;


S0=R0.*pi*2;
V0=R0.^2*pi;
rS=S./(S+S0);
rV=V./(V+V0);
plot(R0,rS,'b')
hold on
plot(R0,rV,'r')
title('varying radius dendrite')
ylabel('fraction of protein in spine')
xlabel('radius of the dendrite [\mu m]')

axis([0 max(R0) 0 1])

subplot(4,2,6)
plot((R0./BASE.R0-1).*100,(rS./rS_ref-1).*100,'b')
hold on
plot((R0./BASE.R0-1).*100,(rV./rV_ref-1).*100,'r')
    axis([-10 +10 -20 20])

title('varying radius dendrite')
xlabel('radius of the dendrite [\mu m]')
ylabel('% variation of protein in synapse')
%%
[l, r, R, R0]=assign_from_base(BASE);
l=0:0.01:4;    %length of the neck;
[rS, rV] = calc_spine(l, r, R, R0);



subplot(4,2,7)

    plot(l,rS,'b')
    hold on
    plot(l,rV,'r')
title('varying length neck')
xlabel('length of spine neck [\mu m]')
ylabel('fraction of protein in spine')


axis([0 4 0 1])

subplot(4,2,8)
    plot((l./BASE.l-1).*100,(rS./rS_ref-1).*100,'b')
    hold on
    plot((l./BASE.l-1).*100,(rV./rV_ref-1).*100,'r')
    axis([-10 +10 -1 1])
title('varying length neck')
xlabel('length of spine neck [\mu m]')
ylabel('% variation of protein in synapse')



function [l, r, R, R0] = assign_from_base(BASE)

    l=BASE.l;
    r=BASE.r;
    R=BASE.R;
    R0=BASE.R0;


end

function [rS,rV] = calc_spine(l, r, R, R0)

    S=2.*pi.*r.*l+4.*pi.*R.^2;
    V=pi.*r.^2.*l+3/4.*pi.*R.^3;
    S0=R0.*pi.*2;
    V0=R0.^2.*pi;

    
    size(S+S0)
    
    rS=S./(S+S0);
    size(S)
    size(S0)
    
    rV=V./(V+V0);
    
end