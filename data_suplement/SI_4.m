function [] = SI_4(path_1,path_2)

    if(exist('all_simu_data.mat')==2)
        load('all_simu_data.mat');
    else
        here=pwd;
        cd ('./../SIMU_DATA')
        IMPORT_SIMU(path_2);
        cd (here);
    end
    
    
    clc
    D=4;
    ra=20;
    dx=8;
    SS=(TREE_S_S(2:D:end,end)./dx);
    CS=(TREE_S_C(2:D:end,end)./dx);
    SO=(TREE_O_S(2:D:end,end)./dx);
    CO=(TREE_O_C(2:D:end,end)./dx);

    SS110=(TREE_S_S_110(2:D:end,end)./dx);
    CS110=(TREE_S_C_110(2:D:end,end)./dx);
    SO110=(TREE_O_S_110(2:D:end,end)./dx);
    CO110=(TREE_O_C_110(2:D:end,end)./dx);



X=TREE_O_C(2:D:end,1);
Y=TREE_O_C(2:D:end,2);
Z=TREE_O_C(2:D:end,3);


M=max([max(SS),max(CS),max(SO),max(CO),max(SS110),max(CS110),max(SO110),max(CO110)]);
m=min([min(SS),min(CS),min(SO),min(CO),min(SS110),min(CS110),min(SO110),min(CO110)]);




SS=SS./m;
SO=SO./m;
CS=CS./m;
CO=CO./m;
SS110=SS110./m;
SO110=SO110./m;
CS110=CS110./m;
CO110=CO110./m;



M=max([max(SS),max(CS),max(SO),max(CO),max(SS110),max(CS110),max(SO110),max(CO110)]);
m=min([min(SS),min(CS),min(SO),min(CO),min(SS110),min(CS110),min(SO110),min(CO110)]);

M=(log10(M));
m=(log10(m));


m=floor(10*m)/10;
M=ceil(10*M)/10;

f=1;

    subplot(2,4,1)
        scatter(X,Y,ra,log10(SS),'filled');
        %colorbar
        axis('equal')
        caxis([m M])

        yticks([])
        xticks([])
        title(['Surf'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[round(10*min(log10(SS)))/10 round(10*max(log10(SS)))/10])
1/min(SS)

    subplot(2,4,2)
        scatter(X,Y,ra,log10(CS),'filled');
        caxis([m M])
        %cticks([-18 -10 -4])
        axis('equal')
        title(['Cyto'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[ceil(10*min(log10(CS)))/10 floor(10*max(log10(CS)))/10])
1/min(CS)
    subplot(2,4,3)
        scatter(X,Y,ra,log10(SO),'filled');
        %colorbar
        axis('equal')
        caxis([m M])

        yticks([])
        xticks([])
        title(['Surf opt'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[round(10*min(log10(SO)))/10 round(10*max(log10(SO)))/10])
1/min(SO)
    

    subplot(2,4,4)
        scatter(X,Y,ra,log10(CO),'filled');
        caxis([m M])
        %cticks([-18 -10 -4])
        axis('equal')
        title(['Cyto opt'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        
        set(cbh,'XTick',sort([floor(10*min(log10(CO)))/10 floor(10*max(log10(CO)))/10]))
1/min(CO) 

        subplot(2,4,5)
        scatter(X,Y,ra,log10(SS110),'filled');
        %colorbar
        axis('equal')
        caxis([m M])

        yticks([])
        xticks([])
        title(['Surf'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[round(10*min(log10(SS110)))/10 round(10*max(log10(SS110)))/10])
1/min(SS110)

    subplot(2,4,6)
        scatter(X,Y,ra,log10(CS110),'filled');
        caxis([m M])
        %cticks([-18 -10 -4])
        axis('equal')
        title(['Cyto'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[ceil(10*min(log10(CS110)))/10 floor(10*max(log10(CS110)))/10])
1/min(CS110)
    subplot(2,4,7)
        scatter(X,Y,ra,log10(SO110),'filled');
        %colorbar
        axis('equal')
        caxis([m M])

        yticks([])
        xticks([])
        title(['Surf opt'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[round(10*min(log10(SO110)))/10 round(10*max(log10(SO110)))/10])
1/min(SO110)
    

    subplot(2,4,8)
        scatter(X,Y,ra,log10(CO110),'filled');
        caxis([m M])
        %cticks([-18 -10 -4])
        axis('equal')
        title(['Cyto opt'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        
        set(cbh,'XTick',sort([floor(10*min(log10(CO110)))/10 floor(10*max(log10(CO110)))/10]))
1/min(CO110) 

    
    
end
