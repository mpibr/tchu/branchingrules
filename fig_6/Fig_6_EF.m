function [] = Fig_6_EF(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    if(exist('all_simu_data.mat')==2)
        load('all_simu_data.mat');
    else
        here=pwd;
        cd ('./../SIMU_DATA')
        IMPORT_SIMU(path_2);
        cd (here);
    end
    
    


    N=200;

    ALL_S_C=ALL_S_C./8;
    ALL_S_S=ALL_S_S./8;
    ALL_O_C=ALL_O_C./8;
    ALL_O_S=ALL_O_S./8;



    m=min([min(ALL_S_C) min(ALL_O_C) min(ALL_S_S) min(ALL_O_S)]);
    M=max([max(ALL_S_C) max(ALL_O_C) max(ALL_S_S) max(ALL_O_S)]);




    Nm=1/M;

    NM=1/m;


    D=(NM/Nm)^(1/N);

    NN=Nm*D.^[0:N];


    FS=zeros(1,N);
    FSO=zeros(1,N);
    FC=zeros(1,N);
    FCO=zeros(1,N);


    for i=1:length(NN)
        FS(i)=length(find(ALL_S_S.*NN(i)>=1))/length(ALL_S_S);
        FSO(i)=length(find(ALL_O_S.*NN(i)>=1))/length(ALL_S_S);
        FC(i)=length(find(ALL_S_C.*NN(i)>=1))/length(ALL_S_S);
        FCO(i)=length(find(ALL_O_C.*NN(i)>=1))/length(ALL_S_S);
    end


    %%
    % 90% range:
    clc

    clc
    disp('__________ 90% ______________ ')

    % 95% range
    INDX_S=find(FS>0.9);    NN(INDX_S(1));
    INDX_SO=find(FSO>0.9);  NN(INDX_SO(1));
    INDX_C=find(FC>0.9);    NN(INDX_C(1));
    INDX_CO=find(FCO>0.9);  NN(INDX_CO(1));


    disp(['Requires N=' num2str(NN(INDX_S(1)))  ' of surface protein'])
    disp(['Requires N=' num2str(NN(INDX_C(1)))  ' of cytoplasm protein'])
    disp(['Requires N=' num2str(NN(INDX_SO(1))) ' of surface optimal protein'])
    disp(['Requires N=' num2str(NN(INDX_CO(1))) ' of cytoplasm optimal protein'])


    disp(['implementing optimality saves: x' num2str((NN(INDX_S(1))/NN(INDX_SO(1)))) '% of surface protein'])
    disp(['implementing optimality saves: x' num2str((NN(INDX_C(1))/NN(INDX_CO(1)))) '% of cytoplasm protein'])



    disp('__________ 100% ______________ ')

    % 95% range
    INDX_S=find(FS>=1);    NN(INDX_S(1));
    INDX_SO=find(FSO>=1);  NN(INDX_SO(1));
    INDX_C=find(FC>=1);    NN(INDX_C(1));
    INDX_CO=find(FCO>=1);  NN(INDX_CO(1));



    disp(['implementing optimality saves: x' num2str((NN(INDX_S(1))/NN(INDX_SO(1)))) '% of surface protein'])
    disp(['implementing optimality saves: x' num2str((NN(INDX_C(1))/NN(INDX_CO(1)))) '% of cytoplasm protein'])



    close all


    figure(2)
    subplot(2,1,1)


    INDX_S=[];
    INDX_SO=[];
    INDX_C=[];
    INDX_CO=[];


    PCG=.6:0.005:1;
    for i=1:length(PCG)
        dumm=find(FS>=PCG(i));          INDX_S(i)=NN(dumm(1));
        dumm=find(FSO>=PCG(i));         INDX_SO(i)=NN(dumm(1));
        dumm=find(FC>=PCG(i));          INDX_C(i)=NN(dumm(1));
        dumm=find(FCO>=PCG(i));         INDX_CO(i)=NN(dumm(1));
    end



    plot(PCG*100,INDX_S,'b')
    hold on
    plot(PCG*100,INDX_SO,'--b')
    plot(PCG*100,INDX_C,'r')
    plot(PCG*100,INDX_CO,'--r')


    subplot(2,1,2)
    plot(PCG*100,100*(INDX_S./INDX_SO-1),'b')
    hold on
    plot(PCG*100,100*(INDX_C./INDX_CO-1),'r')

    
end