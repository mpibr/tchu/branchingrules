function [] = Fig_6_CD(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    if(exist('all_simu_data.mat')==2)
        load('all_simu_data.mat');
    else
        here=pwd;
        cd ('./../SIMU_DATA')
        IMPORT_SIMU(path_2);
        cd (here);
    end
    
    
    clc
D=4;
ra=20;
dx=8;
SS=(TREE_S_S(2:D:end,end)./dx);
CS=(TREE_S_C(2:D:end,end)./dx);
SO=(TREE_O_S(2:D:end,end)./dx);
CO=(TREE_O_C(2:D:end,end)./dx);



X=TREE_O_C(2:D:end,1);
Y=TREE_O_C(2:D:end,2);
Z=TREE_O_C(2:D:end,3);


M=max([max(SS),max(CS),max(SO),max(CO)]);
m=min([min(SS),min(CS),min(SO),min(CO)]);




SS=SS./m;
SO=SO./m;
CS=CS./m;
CO=CO./m;


M=max([max(SS),max(CS),max(SO),max(CO)]);	M=(log10(M));
m=min([min(SS),min(CS),min(SO),min(CO)]);	m=(log10(m));


m=floor(10*m)/10;
M=ceil(10*M)/10;

f=1;

    subplot(1,4,1)
        scatter(X,Y,ra,log10(SS),'filled');
        %colorbar
        axis('equal')
        caxis([m M])

        yticks([])
        xticks([])
        title(['Surf'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[round(10*min(log10(SS)))/10 round(10*max(log10(SS)))/10])
1/min(SS)

    subplot(1,4,2)
        scatter(X,Y,ra,log10(CS),'filled');
        caxis([m M])
        %cticks([-18 -10 -4])
        axis('equal')
        title(['Cyto'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[ceil(10*min(log10(CS)))/10 floor(10*max(log10(CS)))/10])
1/min(CS)
    subplot(1,4,3)
        scatter(X,Y,ra,log10(SO),'filled');
        %colorbar
        axis('equal')
        caxis([m M])

        yticks([])
        xticks([])
        title(['Surf opt'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        set(cbh,'XTick',[round(10*min(log10(SO)))/10 round(10*max(log10(SO)))/10])
1/min(SO)
    

    subplot(1,4,4)
        scatter(X,Y,ra,log10(CO),'filled');
        caxis([m M])
        %cticks([-18 -10 -4])
        axis('equal')
        title(['Cyto opt'])
        set(gca,'fontsize',30*f)
        yticks([])
        xticks([])
        cbh=colorbar;
        
        set(cbh,'XTick',sort([floor(10*min(log10(CO)))/10 floor(10*max(log10(CO)))/10 2:12]))
1/min(CO) 
    
    
    
end
