function [] = Fig_6_B(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    if(exist('all_simu_data.mat')==2)
        load('all_simu_data.mat');
    else
        here=pwd;
        cd ('./../SIMU_DATA')
        IMPORT_SIMU(path_1);
        cd (here);
    end
    
    
    plot(X_A_0_C,Y_A_0_S./Y_A_0_C)
    hold on
    plot(X_A_0_C,Y_A_2_S./Y_A_2_C)
    plot(X_A_0_C,Y_A_8_S./Y_A_8_C)
    plot(X_Sym_C,Y_Sym_S./Y_Sym_C)
    
    
    axis([0 500 0 4])
    
    
    
    
    
end

