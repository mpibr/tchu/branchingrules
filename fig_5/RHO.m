function [rho] = RHO(r1,r2,gamma,I,L0,L1,L2,lambda)
    %gives the density at the tip of the I-th daughter dendrite.
    % it assumes r1=r1, r2=r2, gamma=1/2 for surface or cytoplasm, L0, L1,
    % L2 and lambda.



    b=1./lambda;


    if(I==1)
        N=r1.^gamma.*cosh(b.*L2);
    elseif(I==2)
        N=r2.^gamma.*cosh(b.*L1);
    else
        disp('ERROR');
    end

    c0=cosh(L0.*b);
    c1=cosh(L1.*b);
    c2=cosh(L2.*b);
    s0=sinh(L0.*b);
    s1=sinh(L1.*b);
    s2=sinh(L2.*b);
    
    
    D=lambda.*(r1.*c0.*c2.*s1+c1.*(c2.*s0+r2.*c0.*s2));
    
    rho=N./D;



end