function [] = Fig_5_B(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    
    
    RR=[ALI.R1./ALI.R0 ALI.R2./ALI.R0];

    r2=median(RR);



    r1=0:0.01:1.5;
    L0=100;
    L1=50;
    L2=25;
    lambda=110;




    gamma=1;
    I=1;
    T1s=RHO(r1,r2,gamma,I,L0,L1,L2,lambda);
    I=2;
    T2s=RHO(r1,r2,gamma,I,L0,L1,L2,lambda);

    gamma=2;
    I=1;
    T1c=RHO(r1,r2,gamma,I,L0,L1,L2,lambda);
    I=2;
    T2c=RHO(r1,r2,gamma,I,L0,L1,L2,lambda);


    plot(r1,T1s,'b')
    hold on
    plot(r1,T1c,'--b')

    plot(r2,0.006,'o')
    
    
end