function [] = Fig_5_E(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    

    % we call 1 the longer dendrite, and 2 the shorter
    LL1=ALI.L1(ALI.L1>0 & (ALI.L1<100000));
    LL2=ALI.L2(ALI.L1>0 & (ALI.L1<100000));
    RR1=ALI.R1(ALI.L1>0 & (ALI.L1<100000))./ALI.R0(ALI.L1>0 & (ALI.L1<100000));
    RR2=ALI.R2(ALI.L1>0 & (ALI.L1<100000))./ALI.R0(ALI.L1>0 & (ALI.L1<100000));


    RR=[RR1 RR2];

%m=10;M=1000; N=100; Delta= (M/m)^(1/N); lambdas=m.*Delta.^(1:N);

lambdas=6:0.25:10000;


NS=[];
NC=[];


    %for each vaule of lambda (L-loop) we calculate the Number of proteins needed to
    %provide 1 protein/termination NREALS/C and then we compare this quantity
    %with the optimal number of proteins NoptS/C. We do that for each
    %branch (i-loop) and we take the median across the branches.
    
for L=1:length(lambdas)
    if(mod(L,100)==0)
        disp([num2str(L) 'of ' num2str(length(lambdas))])
    end
	lambda=lambdas(L);
	
    S=zeros(1,length(LL1));
    C=zeros(1,length(LL1));
    
    for i=1:length(LL1)
%         i
        L1=LL1(i);
        L2=LL2(i);
        R1=RR1(i);
        R2=RR2(i);
        R1_opt=median(RR);
        R2_opt_s=R1_opt*(cosh(L2/lambda)/cosh(L1/lambda));
        R2_opt_c=R1_opt*(cosh(L2/lambda)/cosh(L1/lambda))^(0.5);
        
        
        
        Nopts=1./RHO(R1_opt,R2_opt_s,1,1,1,L1,L2,lambda);
        
        N1reals=1./RHO(RR1(i),RR2(i),1,1,1,L1,L2,lambda);
        N2reals=1./RHO(RR1(i),RR2(i),1,2,1,L1,L2,lambda);
        NREALS=max(N1reals,N2reals);

        
        Noptc=1./RHO(R1_opt,R2_opt_s,2,1,1,L1,L2,lambda);
        
        N1realc=1./RHO(RR1(i),RR2(i),2,1,1,L1,L2,lambda);
        N2realc=1./RHO(RR1(i),RR2(i),2,2,1,L1,L2,lambda);
        NREALC=max(N1realc,N2realc);

        S(i)=NREALS/Nopts;
        C(i)=NREALC/Noptc;
    end
    medS=median(S);
    medC=median(C);

    NS=[NS,medS];
    NC=[NC,medC];

end
NS=NS./min(NS);
NC=NC./min(NC);

semilogy(lambdas,NS,'b')
hold on
semilogy(lambdas,NC,'--r')



plot(65,1,'or')
plot(110,1.25,'ob')
plot(397,1,'ok')
    
end