function [] = Fig_5_C(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    
    
    LS=(ALI.Ls);
    LC=(ALI.Lc);
    LP=protein_lambdas;

    dx=0.4;

        LL=0:dx:4.5;
        [ys,xs]=hist(log10(LS),LL);
        [yc,xc]=hist(log10(LC),LL);
        [yb,xb]=hist(log10(LP),LL);
    figure(2)
        X=[xs; xc; xb]';
        Y=[ys; yc; yb]';
        bar(X,Y,1.3)
        hold on
        % 
        Y=quantile(log10(LS),[.25 .50 .75]);
        errorbar(Y(2),1,(Y(2)-Y(1)),Y(3)-Y(2),'horizontal');


        Y=quantile(log10(LC),[.25 .50 .75]);
        errorbar(Y(2),2,(Y(2)-Y(1)),Y(3)-Y(2),'horizontal');

        Y=quantile(log10(LP),[.25 .50 .75]);
        errorbar(Y(2),3,(Y(2)-Y(1)),Y(3)-Y(2),'horizontal');
        axis([-.2 4.25 0 9])

    
    
end