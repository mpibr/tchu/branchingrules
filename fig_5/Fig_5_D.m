function [] = Fig_5_D(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    
    LL1=ALI.L1(ALI.L1>0 & ALI.L1<1000);
    LL2=ALI.L2(ALI.L1>0 & ALI.L1<1000);
    L1=max(LL1,LL2);
    L2=min(LL1,LL2);
    indx_MAX = find(L1./L2==max(L1./L2));

    L1_M=L1(indx_MAX);
    L2_M=L2(indx_MAX);

    L3=L1(indx_MAX);
    L1=L2(indx_MAX);
    L2=L3;


    LP=protein_lambdas;

    lambda=median(LP);
    lambda=110;
    RR=[ALI.R1./ALI.R0 ALI.R2./ALI.R0];
    R2=median(RR);



    R1=0:0.0001:3;

    N_1S=1./RHO(R1,R2,1,1,1,L1,L2,lambda);
    N_2S=1./RHO(R1,R2,1,2,1,L1,L2,lambda);
    N_1C=1./RHO(R1,R2,2,1,1,L1,L2,lambda);
    N_2C=1./RHO(R1,R2,2,2,1,L1,L2,lambda);

    NS=max(N_1S,N_2S);
    NC=max(N_1C,N_2C);


    figure(2)

    indx_s=find(NS==min(NS));
    R1_opt_s=R1(indx_s);
    %N_OPT_s=1./RHO(R1_opt_s,R2,1,1,1,L1,L2,lambda);


    indx_c=find(NC==min(NC));
    R1_opt_c=R1(indx_c);
    %N_OPT_c=1./RHO(R1_opt_c,R2,2,1,1,L1,L2,lambda);

    fr=0.5:0.001:1.5;

    for i=1:length(fr)
        R1_all_s=R1_opt_s.*fr;
        extra_s1=1./RHO(R1_all_s,R2,1,1,1,L1,L2,lambda);
        extra_s2=1./RHO(R1_all_s,R2,1,2,1,L1,L2,lambda);
        E_s=max(extra_s1,extra_s2);

        R1_all_c=R1_opt_c.*fr;
        extra_c1=1./RHO(R1_all_c,R2,2,1,1,L1,L2,lambda);
        extra_c2=1./RHO(R1_all_c,R2,2,2,1,L1,L2,lambda);
        E_c=max(extra_c1,extra_c2);

    end


    figure(2)
    plot(fr*100-100,E_s./min(E_s).*100,'k')
    hold on
    plot(fr*100-100,E_c./min(E_c).*100,'--k')

    
    
end