function [] = Fig_3_E(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    r=0:0.01:1.5;

    A=zeros(length(r));

    for i=1:length(r)

        for j=1:length(r)
            A(i,j)=((r(i)+r(j))./(r(i)^2+r(j)^2)-1).*1;
        end
    end


    maxR=1;
    A(A>maxR)=maxR;

    RC1=max(R1_C./R0_C,R2_C./R0_C);
    RC2=min(R1_C./R0_C,R2_C./R0_C);



    imagesc(r,1.5-r,A);
    hold on
    plot(RC1,1.5-RC2,'o')

end

