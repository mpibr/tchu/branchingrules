function [] = Fig_3_C(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    r=0:0.01:1.5;

    rp=[ALI.R1./ALI.R0 ALI.R2./ALI.R0];
    ro=[OTO.R1./OTO.R0; OTO.R2./OTO.R0];
    rc=[R1_C./R0_C R2_C./R0_C];


    RPyr=median(rp);
    ROTO=median(ro);
    Rcul=median(rc);

    RS=2.*r;
    RC=2.*r.^2;

    DR=(RS./RC-1);
    BR=(RS./RC-1);

    plot(r,RS,'k');
    hold on
    plot(r,RC,'--k');

    yyaxis right

    plot(r,DR,'g');
    axis([0 1.5 -.4 5])

end

