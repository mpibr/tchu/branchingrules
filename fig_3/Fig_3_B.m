function [] = Fig_3_B(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    
    r1=0:0.001:1;

    alpha_PYR=2.2816;
    alpha_OTO=1.00;


    r2_PYR=(1-r1.^alpha_PYR).^(1/alpha_PYR);
    r2_OTO=(1-r1.^alpha_OTO).^(1/alpha_OTO);




    P12sP=(r1+r2_PYR)./(1+r1+r2_PYR);
    P12cP=(r1.^2+r2_PYR.^2)./(1+r1.^2+r2_PYR.^2);

    P12sO=(r1+r2_OTO)./(1+r1+r2_OTO);
    P12cO=(r1.^2+r2_OTO.^2)./(1+r1.^2+r2_OTO.^2);


    subplot(2,1,1)
    plot(r1,P12sP,'r-')
    hold on
    plot(r1,P12cP,'r--')

    subplot(2,1,2)
    plot(r1,P12sO,'b')
    hold on
    plot(r1,P12cO,'b--')
    
end

