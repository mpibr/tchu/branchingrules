function [] = Fig_3_F(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    
    
    

    RSP=(ALI.R1+ALI.R2)./ALI.R0;            %Relative ratio for Pyr Surface
    RCP=(ALI.R1.^2+ALI.R2.^2)./ALI.R0.^2;   %Relative ratio for Pyr Cyto
    
	RSO=(OTO.R1+OTO.R2)./OTO.R0;            %Relative ratio for STG Surface
    RCO=(OTO.R1.^2+OTO.R2.^2)./OTO.R0.^2;   %Relative ratio for STG CYTO

	RSC=(R1_C+R2_C)./R0_C;                  %Relative ratio for CULT SURF
    RCC=(R1_C.^2+R2_C.^2)./R0_C.^2;         %Relative ratio for CULT CYTO

    R_R=RSP./RCP-1;                         %Surface bias for PYR
    R_O=RSO./RCO-1;                         %Surface bias for STG
    R_C=RSC./RCC-1;                         %Surface bias for CULT
    
    
    

    %bootstrapped relative ratio (for reference numbers, not figure)
    MADs=bootstrp(1000,@mean,RSP);
    MADc=bootstrp(1000,@mean,RCP);

	MODs=bootstrp(1000,@mean,RSO);
    MODc=bootstrp(1000,@mean,RCO);
    
    MCDs =bootstrp(1000,@mean,RSC);
    MCDc =bootstrp(1000,@mean,RCC);
    
	%bootstrapped surface bias
    RAm=bootstrp(1000,@mean,R_R);
    ROm=bootstrp(1000,@mean,R_O);
    RCm=bootstrp(1000,@mean,R_C);

    
	Y_Pr=quantile(R_R,[.25 .5 .75]);
    Y_Or=quantile(R_O,[.25 .5 .75]);
    Y_Cr=quantile(R_C,[.25 .5 .75]);





    

    



    
    
    
    [mean(MADs) std(MADs) mean(MADc) std(MADc)]
    display('---------------------')
    display('---------------------')    
    [mean(MODs) std(MODs) mean(MODc) std(MODc)]
    display('---------------------')
    display('---------------------')
    [mean(MCDs) std(MCDs) mean(MCDc) std(MCDc)]





    m=-1;
    M=2;
    d=0.125;
    R=m-d:d:M+d;
    [y,x]=hist(R_R,R);
    plot(x,y./sum(y),'r');
    hold on
    [y,x]=hist(R_O,R);
    plot(x,y./sum(y),'b');
    [y,x]=hist(R_C,R);
    plot(x,y./sum(y),'k');


    errorbar(mean(RAm),.2,std(RAm),std(RAm),'horizontal','r')
    errorbar(Y_Pr(2), .21, Y_Pr(2)-Y_Pr(1),Y_Pr(3)-Y_Pr(2),'horizontal','r')

    errorbar(mean(ROm),.3,std(ROm),std(ROm),'horizontal','b')
    errorbar(Y_Or(2), .31, Y_Or(2)-Y_Or(1),Y_Or(3)-Y_Or(2),'horizontal','b')

    errorbar(mean(RCm),.1,std(RCm),std(RCm),'horizontal','k')
    errorbar(Y_Cr(2), .11,Y_Cr(2)-Y_Cr(1),Y_Cr(3)-Y_Cr(2),'horizontal','k')


    [mean(RCm) std(RCm)]
end

