function [] = Fig_4_E(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    if(exist('all_fluo.mat')==2)
        load('all_fluo.mat');
    else
        here=pwd;
        cd ('./../FLUO_DATA')
        IMPORT_FLUO(path_1);
        cd (here);
    end
    
    
    
    mm=[0 0 0 0 1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4];
    DD=[1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];

    for i=1:length(mm)
        GFP_R(i).gfp=RES(i).gfp_N(:,4);
        NLG_R(i).nlg=RES(i).ngl_N(:,4);
    end




    for i=1:length(mm)
        LLG(i)=length(GFP_R(i).gfp);
        LLN(i)=length(NLG_R(i).nlg);
    end

    indx=[];
    NMIN=17;

    for i=1:length(mm)
        if(LLG(i)>=NMIN & LLN(i)>=NMIN)
            indx=[indx,i];
        end
    end


    
    for i=1:length(mm)
        GFP_R(i).gfp=RES(i).gfp_N(:,4);
        NLG_R(i).nlg=RES(i).ngl_N(:,4);
        BG=bootstrp(1000,@(x)mean(x),GFP_R(i).gfp);

        MG(i)=mean(BG);
        SG(i)=std(BG);

        BN=bootstrp(1000,@(x)mean(x),NLG_R(i).nlg);

        MN(i)=mean(BN);
        SN(i)=std(BN);
    end

    RSC=(R1_C+R2_C)./R0_C;
    RCC=(R1_C.^2+R2_C.^2)./R0_C.^2;
    r=(RSC./RCC-1);

    for i=1:length(indx)
        G=bootstrp(10000,@mean,GFP_R(indx(i)).gfp);

        c(i) =mean(G);
        dc(i)=std (G);

        G=bootstrp(10000,@mean,NLG_R(indx(i)).nlg);    
        s(i) =mean(G);
        ds(i)=std (G);

        R(i)=s(i)./c(i);

    end


    


    dR=sqrt((ds./c).^2+(s.*dc./c./c).^2);
    R=(R-1);


    errorbar([1:length(R)],R,dR,dR,'v')
    hold on


    BTr=bootstrp(10000,@mean,r);

    me_r=mean(BTr);
    er_r=std(BTr);


    plot([0 21],[me_r me_r],'r')
    plot([0 21],[me_r-er_r me_r-er_r],'r')
    plot([0 21],[me_r+er_r me_r+er_r],'r')

    
end
