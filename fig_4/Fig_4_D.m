function [] = Fig_4_D(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    if(exist('all_fluo.mat')==2)
        load('all_fluo.mat');
    else
        here=pwd;
        cd ('./../FLUO_DATA')
        IMPORT_FLUO(path_1);
        cd (here);
    end
    
    
    
    mm=[0 0 0 0 1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4];
    DD=[1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];

    for i=1:length(mm)
        GFP_R(i).gfp=RES(i).gfp_N(:,4);
        NLG_R(i).nlg=RES(i).ngl_N(:,4);
    end




    for i=1:length(mm)
        LLG(i)=length(GFP_R(i).gfp);
        LLN(i)=length(NLG_R(i).nlg);
    end

    indx=[];
    NMIN=17;

    for i=1:length(mm)
        if(LLG(i)>=NMIN & LLN(i)>=NMIN)
            indx=[indx,i];
        end
    end


    
    for i=1:length(mm)
        GFP_R(i).gfp=RES(i).gfp_N(:,4);
        NLG_R(i).nlg=RES(i).ngl_N(:,4);
        BG=bootstrp(1000,@(x)mean(x),GFP_R(i).gfp);

        MG(i)=mean(BG);
        SG(i)=std(BG);

        BN=bootstrp(1000,@(x)mean(x),NLG_R(i).nlg);

        MN(i)=mean(BN);
        SN(i)=std(BN);
    end

    RS=(R1_C+R2_C)./R0_C;
    RC=(R1_C.^2+R2_C.^2)./R0_C.^2;
    
    bs=bootstrp(1000,@(x)mean(x),RS);
    ms=mean(bs);
    ss=std(bs);

    bc=bootstrp(1000,@(x)mean(x),RC);
    mc=mean(bc);
    sc=std(bc);



    YN=[ms ms];
    YNp=[ms+ss ms+ss];
    YNm=[ms-ss ms-ss];

    YG=[mc mc];
    YGp=[mc+sc mc+sc];
    YGm=[mc-sc mc-sc];

    x=[0 21];

    errorbar([1:length(indx)]',[MG(indx)],[SG(indx)],[SG(indx)],'.r')
    title('bootstrap mean')
    hold on
    plot(x,YG,'r')
    plot(x,YGp,'r')
    plot(x,YGm,'r')

    errorbar([1:length(indx)]'+0.25,[MN(indx)],[SN(indx)],[SN(indx)],'.b')

    plot(x,YN,'b--')
    plot(x,YNp,'b--')
    plot(x,YNm,'b--')

    axis([0 length(indx)+1 1 2.5])
    
end

