function [] = Fig_3_B(path_1,path_2)


    if(exist('all_bio_data.mat')==2)
        load('all_bio_data.mat');
    else
        here=pwd;
        cd ('./../RAW_DATA')
        IMPORT(path_1);
        cd (here);
    end
    if(exist('all_fluo.mat')==2)
        load('all_fluo.mat');
    else
        here=pwd;
        cd ('./../FLUO_DATA')
        IMPORT_FLUO(path_1);
        cd (here);
    end
    
    %plot the histogram of i=3;

    GFP_R.gfp=RES(3).gfp_N(:,4);
    NLG_R.nlg=RES(3).ngl_N(:,4);
    
    RSC=(R1_C+R2_C)./R0_C;
    RCC=(R1_C.^2+R2_C.^2)./R0_C.^2;

    BTC=bootstrp(10000,@mean,RCC);
    BTS=bootstrp(10000,@mean,RSC);

    mc=mean(BTC);
    sc=std(BTC);
    mn=mean(BTS);
    sn=std(BTS);
    
    i=3;
    RR=0:0.1:2.5;


    figure(10)
    
    [yg,xg]=hist(GFP_R.gfp,RR);
    [yn,xn]=hist(NLG_R.nlg,RR);

    
    X=[xg; xn]';
    Y=[yg./sum(yg); yn./sum(yn)]';
    bar(X, Y, 'BarWidth', 2);
    hold on
    
    errorbar(mc,0.25,sc,sc,'horizontal','r')
    hold on
    errorbar(mn,0.25,sn,sn,'horizontal','b')
    
    title(['m=' num2str(mm(i)) ' \Delta=' num2str(DD(i)) ' #gfp=' num2str(length(GFP_R.gfp)) ' #nlg=' num2str(length(NLG_R.nlg))])
    
    
    axis([0.5 2.6 0 0.3])
    
    BTC=bootstrp(10000,@mean,GFP_R.gfp);
    BTS=bootstrp(10000,@mean,NLG_R.nlg);
    errorbar(mean(BTC),0.2,std(BTC),std(BTC),'horizontal','r')
    errorbar(mean(BTS),0.2,std(BTS),std(BTS),'horizontal','b')
    
end

