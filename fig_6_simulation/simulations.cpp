#include "simulations.h"

 
simulations::simulations()
{
}


simulations::~simulations()
{
}


vector<simulations> init_simulations()
{


	vector<vector<string> > NAMES_LIST;	//	First entry name of the parameter file to read		//	Second entry name of the folder where to save everything.
	vector<simulations> RES;

	NAMES_LIST = read_List_parameters();

	for (int i = 0; i < NAMES_LIST.size(); i++) {
		string temp = "PAR_" + NAMES_LIST[i][1] + "/";
		create_folder(temp);

		vector<vector<string> > LIST_SYMM;
		vector<int> indx = { 3 , 5 };
		LIST_SYMM = readout(NAMES_LIST[i][0], "CHR_symm", indx);

		vector<vector<string> > LIST_SWC;
		indx = { 3 };
		LIST_SWC = readout(NAMES_LIST[i][0], "SWC", indx);



		simulations dummy;
		for (int j = 0; j < LIST_SYMM.size(); j++) {
			dummy.init_SYMM(LIST_SYMM[j], temp, NAMES_LIST[i][0]);
			RES.push_back(dummy);
		}


		for (int j = 0; j < LIST_SWC.size(); j++) {

			dummy.init_SWC(LIST_SWC[j], temp, NAMES_LIST[i][0]);
			RES.push_back(dummy);
		}
	}
	cout << "pre-build all the simulation. Now I can start. There are " << RES.size() << " simulations pending" << endl;
	return RES;
};


vector<vector<string> > read_List_parameters()
{
	vector<vector<string> > RES;
	vector<int> indx = {2,3};
 
	RES = readout("List_Parameters.txt", "NEW", indx);

	return RES;
}



void simulations::init_SYMM(vector<string> INFO, string pre_folder, string oldparametername) {
	path = pre_folder + "case_L" + INFO[0] + "_N" + INFO[1]+"/";
	create_folder(path);
	//create the swc file

	swc = "RAW.swc";
	
	ARTIFICIAL_GRAPH TWO;
	L = stod(INFO[0]);
	N = stoi(INFO[1]);
	TWO.init(N,L,path,swc);
	
	WHO = "SYM";

	copy_file(oldparametername, path, "parameter.txt");
	TWO.print();
};
void simulations::init_SWC(vector<string> INFO, string pre_folder, string oldparametername) {

	path = pre_folder + "case_" +INFO[0]+"/";
	create_folder(path);
	swc = "RAW.swc";
	WHO = "RAW";
	copy_file(oldparametername, path, "parameter.txt");
	copy_file(INFO[0], path, swc);
	// copy the swc file
	// copy the Parameter file
};

// ------------------------------------------------- //

void ARTIFICIAL_GRAPH::init(int n, double l, string pth, string sw) {
	N = n;
	L = 1;
	path = pth;
	swc = sw;

	element ROOT;
	ROOT.root = -1;
	ROOT.X = { 0, 0, 0 };
	piece.push_back(ROOT);

		element FIRST;
	FIRST.root = 1;
	FIRST.X = { L,0 };
	FIRST.X = R(0, FIRST.X);
	FIRST.generation = 0;
	FIRST.theta = 0;
		piece.push_back(FIRST);

	FIRST.root = 1;
	FIRST.X = { L,0 };
	FIRST.X = R(3.1416, FIRST.X);
	FIRST.generation = 0;
	FIRST.theta = 3.1416;
		piece.push_back(FIRST);




	add_kayley_banch(0.7854);
	//add_kayley_banch(3.1416, 0.7854);

}

vector<double> ARTIFICIAL_GRAPH::R(double theta,vector<double> X) {
	vector<double> RES;
	RES.push_back(X[0] * cos(theta) + X[1] * sin(theta));
	RES.push_back(X[0] * sin(theta) - X[1] * cos(theta));
	return RES;
}

void ARTIFICIAL_GRAPH::add_kayley_banch(double th) {
	element A;
	A.X.resize(2, 0);
	if (N > 0) {
		for (int gen = 1; gen < N+1; gen++) {
			for (int j = 0; j < piece.size(); j++) {
				if (piece[j].generation == (gen - 1)) {
					double THETA, dx, dy;
					
					THETA = piece[j].theta + th;
					dx = L * cos(THETA);
					dy = L * sin(THETA);

					A.X[0] = piece[j].X[0] + dx;
					A.X[1] = piece[j].X[1] + dy;
					A.generation = gen;
					A.theta = THETA;
					A.root = j+1;
					piece.push_back(A);


					THETA = piece[j].theta - th;
					dx = L * cos(THETA);
					dy = L * sin(THETA);

					A.X[0] = piece[j].X[0] + dx;
					A.X[1] = piece[j].X[1] + dy;
					A.generation = gen;
					A.theta = THETA;
					A.root = j+1;
					piece.push_back(A);

				}
			}
		}
	}

}

void ARTIFICIAL_GRAPH::print(){
	char	line[16384];
	string	str;

	ofstream OUT(path + swc);				// save tree in proper folder

	if (OUT.is_open()) { ; }
	else {
		cout << "FAILED TO CREATE " << swc << endl;
	}

	OUT << 1 << " " << 1 << " " << 0 << " " << 0 << " " << 0 << " " << 1 << " " << -1 << endl;;
	for (unsigned int i = 1; i < piece.size(); i++) {
		OUT << i+1 << " " << 3 << " " << piece[i].X[0] << " " << piece[i].X[1] << " " << 0 << " " << 0.02 << " " << piece[i].root << endl;
	}
}