#include "adjacency_builder.h"



vector<compartment> adjacency_builder::impose_radii(vector<compartment> IN) {

	vector<vector<string> > RADRULES;
	string trigger = "RADIUS_RULE";
	vector<int> indx = { 2 };
	string name = path + parameter;
	RADRULES = readout(name, trigger, indx);
	Rall_Power = 2.45;

	if (RADRULES.size() > 0) {
		if (!RADRULES[0].at(0).compare("RR_SYMM")) {
			cout << "RR_SYMM" << endl;
			IN = init_SYMM(IN);
		}
		if (!RADRULES[0].at(0).compare("RALL_EASY_PYR")) {
			cout << "RALL_EASY_PYR" << endl;
			IN = init_SYMM_PYR(IN);
		}

		if (!RADRULES[0].at(0).compare("RR_OPT_SURF")) {
			cout << "RR_OPT_SURF" << endl;
			IN = init_OPT(IN, 1);
		}
		if (!RADRULES[0].at(0).compare("RR_OPT_CYTO")) {
			cout << "RR_OPT_CYTO" << endl;
			IN = init_OPT(IN, 2);
		}
		
	}
	IN = pyr(IN, 1, 1);
	calc_vol(IN);

	return IN;
}
vector<compartment> adjacency_builder::init_OPT(vector<compartment> IN, int gamma) {
	vector<bifurcations> BIF;
	
	import_lambda();

	BIF = build_backbone(IN);
	BIF = optimize_backbone(BIF, gamma);

	for (unsigned int i = 0; i < BIF.size(); i++) {
		//cout <<i<< "-th piece: r1=" << BIF[i].r1 << " r2=" << BIF[i].r2 << " L1= " << BIF[i].L1 << " L2= " << BIF[i].L2 << endl;
	}

	IN = implement_radii(IN, BIF);


	return IN;
}
vector<compartment> adjacency_builder::pyr(vector<compartment> IN, double R_A, double R_B) {

	for (unsigned int i = 0; i < IN.size(); i++) {
		if (IN[i].G.ID == 3) {
			IN[i].G.radius = IN[i].G.radius*R_B;
		}
		if (IN[i].G.ID == 4) {
			IN[i].G.radius = IN[i].G.radius*R_A;
		}
	}


	return IN;
}


void adjacency_builder::import_lambda() {

	vector<vector<string> > RULES;
	vector<string> TR;
	TR.push_back("TRANS");
	TR.push_back("RULE");
	RULES = readout(path + parameter, TR);


	double D, T12;


	if (!RULES[0].at(0).compare("TRANS")) {
		if (!RULES[0].at(2).compare("DIFFUSION_RADIUS_INSIDE")) {
			T12 = time_converter(RULES[0].at(8), RULES[0].at(9));
			D = stod(RULES[0].at(6));
		}
		else if (!RULES[0].at(2).compare("DIFFUSION_RADIUS_SURFACE")) {
			T12 = time_converter(RULES[0].at(8), RULES[0].at(9));
			D = stod(RULES[0].at(6));
		}
		else if (!RULES[0].at(2).compare("DIFFUSION_SURF_LT")) {
			T12 = time_converter(RULES[0].at(8), RULES[0].at(9));
			D = stod(RULES[0].at(6));
		}
		else if (!RULES[0].at(2).compare("DIFFUSION_CYTO_LT")) {
			T12 = time_converter(RULES[0].at(8), RULES[0].at(9));
			D = stod(RULES[0].at(6));
		}
		else { cout << "NOT IMPLEMENTED YET" << endl; }
	}
	else if (!RULES[0].at(0).compare("RULE")) {
		if (!RULES[0].at(1).compare("DIFF_CYTO")) {
			T12 = time_converter(RULES[0].at(6), RULES[0].at(7));
			D = stod(RULES[0].at(3));
		}
		else if (!RULES[0].at(1).compare("DIFF_SURF")) {
			T12 = time_converter(RULES[0].at(6), RULES[0].at(7));
			D = stod(RULES[0].at(3));
		}
		else if (!RULES[0].at(1).compare("DIFFUSION_SURF_LT")) {
			T12 = time_converter(RULES[0].at(6), RULES[0].at(7));
			D = stod(RULES[0].at(3));
		}
		else if (!RULES[0].at(1).compare("DIFFUSION_CYTO_LT")) {
			T12 = time_converter(RULES[0].at(6), RULES[0].at(7));
			D = stod(RULES[0].at(3));
		}
		else { cout << "NOT IMPLEMENTED YET" << endl; }
	}


	double LAMBDA = sqrt(D*T12 / log(2));

	lambda = LAMBDA;
}

bifurcations update_mother(vector<compartment> IN, int pz) {
	bifurcations dummy;
	dummy.indx_ME = pz;

	int indxpre = IN[pz].C.indx_root;
	dummy.L0 = IN[pz].G.dist_prec;
	while (IN[indxpre].C.Num_post == 1) {
		dummy.L0 = dummy.L0 + IN[indxpre].G.dist_prec;
		indxpre = IN[indxpre].C.indx_root;
	}
	dummy.indx_Mother = indxpre;

	return dummy;
}
bifurcations update_daughter(bifurcations dummy, vector<compartment> IN, int pz, int WHO) {
	int indx_post = IN[pz].C.List_post[WHO];
	if (WHO == 0) {
		dummy.first_Daughter_1 = indx_post;
		dummy.L1 = IN[indx_post].G.dist_prec;
	}
	if (WHO == 1) {
		dummy.first_Daughter_2 = indx_post;
		dummy.L2 = IN[indx_post].G.dist_prec;
	}
	while (IN[indx_post].C.Num_post == 1) {
		indx_post = IN[indx_post].C.List_post[0];
		if (WHO == 0) {
			dummy.L1 = dummy.L1 + IN[indx_post].G.dist_prec;
			dummy.indx_Daughter1 = indx_post;
		}
		if (WHO == 1) {
			dummy.L2 = dummy.L2 + IN[indx_post].G.dist_prec;
			dummy.indx_Daughter2 = indx_post;
		}
	}
	return dummy;
}
vector<bifurcations> adjacency_builder::build_backbone(vector<compartment> IN) {
	vector<bifurcations> indx;

	bifurcations dummy;

	for (unsigned int pz = 1; pz < IN.size(); pz++) {
		if (IN[pz].C.Num_post == 2) {


			dummy = update_mother(IN, pz);
			dummy = update_daughter(dummy, IN, pz, 0);
			dummy = update_daughter(dummy, IN, pz, 1);

			//


			indx.push_back(dummy);
		}
	}
	if (indx.size() > 0) {
		indx[0].generation = 0;

		for (int i = 1; i < indx.size(); i++) {
			for (int j = 0; j < indx.size(); j++) {
				if (indx[i].indx_Mother == indx[j].indx_ME) {
					indx[i].generation = indx[j].generation + 1;
				}
			}
		}

		int MG = 0;
		for (int i = 0; i < indx.size(); i++) { if (indx[i].generation > MG) { MG = indx[i].generation; } }

		for (int i = 0; i < indx.size(); i++) {
			indx[i].generation = MG - indx[i].generation;
		}
	}
	return indx;
}
vector<compartment> adjacency_builder::implement_radii(vector<compartment> IN, vector<bifurcations> BIF) {

	for (unsigned int pz = 0; pz < IN.size(); pz++) { IN[pz].G.radius = 1; }


	for (unsigned int pz = 1; pz < IN.size(); pz++) {
		int root = IN[pz].C.indx_root;
		if (root == 0) {
			IN[pz].G.radius = IN[root].G.radius;
		}
		else {
			if (IN[root].C.Num_post == 1) {
				IN[pz].G.radius = IN[root].G.radius;
			}
			if (IN[root].C.Num_post == 2) {
				int BB_root;
				for (unsigned int ii = 0; ii < BIF.size(); ii++) {
					if (BIF[ii].indx_ME == root) {
						BB_root = ii;
					}
				}
				if (BIF[BB_root].first_Daughter_1 == pz) {
					IN[pz].G.radius = IN[root].G.radius*BIF[BB_root].r1;
				}
				if (BIF[BB_root].first_Daughter_2 == pz) {
					IN[pz].G.radius = IN[root].G.radius*BIF[BB_root].r2;
				}
			}

		}

	}
	return IN;
}
vector<bifurcations> adjacency_builder::optimize_backbone(vector<bifurcations> BIF, int gamma) {
	if (BIF.size() > 0) {
		int MG = 0;
		for (int i = 0; i < BIF.size(); i++) { if (BIF[i].generation > MG) { MG = BIF[i].generation; } }


		vector<double> INFO;	// 3 elements:			0->r1	1->r2	2->Leff
		for (int i = 0; i <= MG; i++) {
			for (int j = 0; j < BIF.size(); j++) {
				if (BIF[j].generation == i) {
					INFO = calc_optimal(BIF[j], gamma);
					//cout << INFO[0] << " " << INFO[1] << " " << INFO[2] << endl;
					//int aa; cin >> aa;
					//cout << "INFOS:" << INFO[0] << " " << INFO[1] << " " << INFO[2] << endl;

					//cout << "__________" << BIF[j].indx_ME;
					for (unsigned int k = 0; k < BIF.size(); k++) {
						//cout << " " << BIF[k].indx_Daughter1 << " " << BIF[k].indx_Daughter2;
					}
					for (int k = 0; k < BIF.size(); k++) {

						if (BIF[k].indx_Daughter1 == BIF[j].indx_ME) {

							BIF[k].L1 = INFO[2];
						}
						if (BIF[k].indx_Daughter2 == BIF[j].indx_ME) {

							BIF[k].L2 = INFO[2];
						}
					}
					BIF[j].r1 = INFO[0];
					BIF[j].r2 = INFO[1];
				}
			}
		}
	}

	//	cout << "optimized" << endl;
	return BIF;
}


vector<double> adjacency_builder::calc_optimal(bifurcations BIF, int gamma) {
	vector<double> RES;
	//cout << "lambda=" << lambda << " L1=" << BIF.L1 << " L2=" << BIF.L2 << " gamma=" << gamma << endl;
	RES.push_back(optimal_r1(lambda, BIF.L1, BIF.L2, gamma));
	//cout << "lambda=" << lambda << " L1=" << BIF.L1 << " L2=" << BIF.L2 << " gamma=" << gamma << endl;
	RES.push_back(pow(1 - pow(RES[0], Rall_Power), 1. / Rall_Power));
	//cout << "lambda=" << lambda << " L1=" << BIF.L1 << " L2=" << BIF.L2 << " gamma=" << gamma << endl;

	RES.push_back(effective_L(lambda, BIF.L0, BIF.L1, BIF.L2, RES[0], RES[1], gamma));
	//cout << BIF.indx_ME << endl;
	return RES;
}


double adjacency_builder::effective_L(double lambda, double L0, double L1, double L2, double r1, double r2, double gamma) {

	//	This function calculate the effective length of a terminal (or virtually terminal) branch. 
	//	Virtually terminal means that further branches already went through this collapsing procedure.

	// if you wonder why all this stuff, see the mathematica file.



	// All of these are just nickname to keep the equation shorter
	double b = 1 / lambda;
	double e0 = exp(b*L0);
	double e1 = exp(b*L1);
	double e2 = exp(b*L2);
	double e01 = exp(b*(L0 + L1));
	double e02 = exp(b*(L0 + L2));
	double e12 = exp(b*(L2 + L1));
	double e012 = exp(b*(L0 + L2 + L1));
	double e0_2 = exp(2 * b*L0);
	double e1_2 = exp(2 * b*L1);
	double e2_2 = exp(2 * b*L2);
	double e01_2 = exp(2 * b*(L0 + L1));
	double e02_2 = exp(2 * b*(L0 + L2));
	double e12_2 = exp(2 * b*(L2 + L1));
	double e012_2 = exp(2 * b*(L0 + L2 + L1));

	double p1 = 1.0*pow(r1,gamma) / (1.0 + pow(r1, gamma) + pow(r2, gamma));
	double p2 = 1.0*pow(r2,gamma) / (1.0 + pow(r1, gamma) + pow(r2, gamma));


	//The equations:

	double piece1 = 1.0 / (2*(-4*e01*p1*(1+e2_2)));
	double piece2_1 = -2 - 2 * e0_2 - 2 * e1_2 - 2*e01_2;
	double piece2_2 = -2 * e2_2 - 2 * e02_2 - 2 * e12_2 - 2 * e012_2;
	double piece2_3 = (+4 * e0_2 + 4 * e1_2 + 4 * e02_2 + 4 * e12_2)*p1;
	double piece2_4 = (+4 * e0_2 + 4 * e01_2 + 4 * e2_2 + 4 * e12_2)*p2;
	double piece2 = piece2_1 + piece2_2 + piece2_3 + piece2_4;
	double piece3_1 = -4 * pow(-4 * e01*p1 - 4*e01*e2_2*p1, 2);
	double piece3_2 = pow(-piece2, 2);
	double piece3 = -sqrt(piece3_1 + piece3_2);

	double L_EFF = lambda * log(piece1*(piece2 + piece3));

	//cout << "lambda=" << lambda << endl;

	//cout << "piece1=" << piece1 << endl;
	//cout << "piece2_1=" << piece2_1 << endl;
	//cout << "piece2_2=" << piece2_2 << endl;
	//cout << "piece2_3=" << piece2_3 << endl;
	//cout << "piece2_4=" << piece2_4 << endl;
	//cout << "piece2=" << piece2 << endl;
	
	//cout << "piece3_1=" << -4 * e01*p1 - 4 * e01*e2_2 << " ____ " << piece3_1 << endl;
	//cout << "piece3_2=" << piece3_2 << endl;
	//cout << "piece3=" << piece3 << endl;



	//cout << "P1=" << p1 << " P2=" << p2 << endl;
	//cout <<"GAMMA="<<gamma<< "L_EFF=" << L_EFF << endl;

	//int a; cin >> a;

	return L_EFF;
}

double adjacency_builder::optimal_r1(double lambda, double L1, double L2, int gamma) {
	//cout << "lambda=" <<lambda<< "L1=" << L1 << " L2=" << L2 << endl;
	//int a; cin >> a;

	double res = 0;
	//cout << "optimal gamma="<<gamma << endl;
	double error = 1e-3;
	double rm = 0;
	double rM = 1;
	double r1 = 0.5*(rm + rM);
	double r2 = pow(1 - pow(r1, Rall_Power), 1. / Rall_Power);
	double VALUE = cosh(L1 / lambda) / cosh(L2 / lambda);
	//cout << "GAMMA==" << gamma << endl;
	double ERR = VALUE - pow(r1 / r2, gamma);

	int count = 0;

	while (abs(ERR)>error) {
		//cout << "r1="<<r1 << " ERR=" << ERR << endl;
		//cin >> a;

		

		if (ERR > 0) {
			rm = r1;
			r1 = 0.5*(rm + rM);
			r2 = pow(1 - pow(r1, Rall_Power), 1. / Rall_Power);
			ERR = VALUE - pow(r1 / r2, gamma);
		}
		if (ERR<0) {
			rM = r1;
			r1 = 0.5*(rm + rM);
			r2 = pow(1 - pow(r1, Rall_Power), 1. / Rall_Power);
			ERR = VALUE - pow(r1 / r2, gamma);
		}
		//cout << " __ " <<ERR<< endl;
	}
	res = r1;
	return res;
}


vector<compartment> adjacency_builder::init_SYMM(vector<compartment> IN) {
	double factor = pow(2, -1./Rall_Power);
	IN[0].G.radius = 1;
	for (int pz = 0; pz < IN.size() - 1; pz++) {
		if (IN[pz].C.Num_post == 1) {
			IN[IN[pz].C.List_post[0]].G.radius = IN[pz].G.radius;
		}
		if (pz == 0) {
			IN[IN[pz].C.List_post[0]].G.radius = IN[pz].G.radius;
		}
		else if (IN[pz].C.Num_post>1) {
			for (int k = 0; k < IN[pz].C.Num_post; k++) {
				IN[IN[pz].C.List_post[k]].G.radius = IN[pz].G.radius*factor;
			}
		}
	}
	return IN;
}
vector<compartment> adjacency_builder::init_SYMM_PYR(vector<compartment> IN) {
	double factor = pow(2, -1. / Rall_Power);
	IN[0].G.radius = 1;
	for (int pz = 0; pz < IN.size() - 1; pz++) {
		if (IN[pz].C.Num_post == 1) {
			IN[IN[pz].C.List_post[0]].G.radius = IN[pz].G.radius;
		}
		else if (IN[pz].C.Num_post>1) {
			for (int k = 0; k < IN[pz].C.Num_post; k++) {
				IN[IN[pz].C.List_post[0]].G.radius = IN[pz].G.radius*factor;
			}
		}
	}

	for (int pz = 0; pz < IN.size(); pz++) {
		if (IN[pz].G.ID == 3) { IN[pz].G.radius = IN[pz].G.radius * 0.6; }
		if (IN[pz].G.ID == 4) { IN[pz].G.radius = IN[pz].G.radius * 1.8; }
	}

	return IN;
}

void adjacency_builder::calc_vol(vector<compartment> &IN) {
	double VOL = 0;
	double SURF = 0;

	for (unsigned int i = 1; i < IN.size(); i++) {
		VOL = VOL + pow(IN[i].G.radius, 2)*IN[i].G.dist_prec;
		SURF = SURF + pow(IN[i].G.radius, 1)*IN[i].G.dist_prec;
	}
	cout << "VOL=" << VOL << endl;
	cout << "SURF=" << SURF << endl;

	for (unsigned int i = 0; i < IN.size(); i++) {
		IN[i].G.VOL = VOL;
		IN[i].G.SURF = SURF;
	}
}
