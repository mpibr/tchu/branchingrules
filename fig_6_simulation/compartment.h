#pragma once

 
	#include "rule.h"
	#include <vector>

	using namespace std;

	class rule;

	struct geometry {
		vector<double> X;
		double radius;
		double dist_prec;
		double dist_soma;
		int ID;
		double VOL;
		double SURF;
	};

	struct building_utilities
	{
		int indx_in_the_upgraded_version;
	};


	struct connectivity {
		vector<int> List_adj;
		vector<int> List_post;
		int Num_adj;
		int Num_post;
		int indx_self;
		int indx_root;
	};


	class compartment {
	public:
		vector<rule*> RULES;
		void init(vector<double> X, int ID, double radius, int parent);

		geometry G;
		connectivity C;
		building_utilities BU;
	private:
	};


