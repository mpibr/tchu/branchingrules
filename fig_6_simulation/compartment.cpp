#include "compartment.h"
 
#include <iostream>
using namespace std;

void compartment::init(vector<double> X, int ID, double radius, int parent) {
	G.X.resize(3);
	G.ID = ID;
	G.X[0] = X[0];
	G.X[1] = X[1];
	G.X[2] = X[2];
	G.radius = radius;
	C.indx_root = parent;
}