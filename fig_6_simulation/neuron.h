#pragma once
 




#pragma once

#include "simulations.h"
#include "compartment.h"
#include "adjacency_builder.h"
#include "rule.h"
#include "teacher.h"
#include "diff_soluble.h"
#include "diff_SURF.h"
#include "diff_LP_SURF.h"
#include "diff_LP_cyto.h"

#include "utilities.h"
#include "pass_structs.h"

using namespace std;




struct timeconstrains
{
	double T_tot;
	double T_print;
	int N_print;
	double precision;

	string CONDITION;
};

class neuron
{
public:
	neuron(simulations param);
	~neuron();
	void evolve();
private:
	vector<compartment> GRAPH;
	string path;
	string swc;
	string parameter;

	void set_temporal_costrain();
	timeconstrains TT;
	double dt;
	double dl;

	vector<TC> import_cathegories();
	void implement_cathegories(vector<TC> classes);

	void teach_DIFF_CYTO(double dt, TC rules, double dL);
	void teach_DIFF_SURF(double dt, TC rules, double dL);

	void teach_DIFF_SURF_LT(double dt, TC rules, double dL);
	void teach_DIFF_CYTO_LT(double dt, TC rules, double dL);

	
	
	void print(vector<compartment> IN, string name);
	bool condition(long unsigned int i,double MM);




	void give_all();
	void take_all();
	void decay_and_create_all();
};
