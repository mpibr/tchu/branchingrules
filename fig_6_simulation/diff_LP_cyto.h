#pragma once
#include "rule.h"
#include "compartment.h"
#include <vector>
#include <iostream>
#include "teacher.h"
 
#include <cmath>

#include "pass_structs.h"

using namespace std;
class diff_LP_CYTO : public rule
{
public:
	diff_LP_CYTO(pass_parameter_SURF_LT &PARAM);


	void give();
	void take(vector<compartment> &GRAPH, int indx);
	void decay_and_create();
	double tell_number() { return N_protein; };
	~diff_LP_CYTO() { ; };
	string tell_name() { return ID; }




private:
	int indx_self;

	double N_protein;
	vector<double> gifts;
	TC my_rule;


	double f;
	double f2;		//fraction decay/creation;
	double f3;

	vector<int> IN;
	vector<int> OUT;
	vector<double> fraction;

	void set_IN(compartment &GRAPH);

	void set_FRACTIONS(pass_parameter_SURF_LT PARAM);
};
