#include "adjacency_builder.h"

 
adjacency_builder::adjacency_builder(string ext_path, string ext_swc) {

	path = ext_path;
	swc = ext_swc;
	parameter = "parameter.txt";
	vector<compartment> RAW_MORPH;
	RAW_MORPH = import_raw_morphology();		//cout << "imported moprhology" << endl;
	RAW_MORPH = recenter(RAW_MORPH);			//cout << "recentered" << endl;
	RAW_MORPH = do_connectivity(RAW_MORPH);		//cout << "connectivity" << endl;
	RAW_MORPH = calc_distances(RAW_MORPH);		//cout << "distances" << endl;
	RAW_MORPH = cut(RAW_MORPH);					//cout << "cut" << endl;
	RAW_MORPH = regularize_graph(RAW_MORPH);	//cout << "regularized" << endl\;
	RAW_MORPH = do_connectivity(RAW_MORPH);		//cout << "connectivity" << endl;
	RAW_MORPH = calc_distances(RAW_MORPH);		//cout << "distances" << endl;
	RAW_MORPH = impose_radii(RAW_MORPH);		//cout << "radii" << endl;
	raw_graph = RAW_MORPH;
	//print_real_rad(RAW_MORPH, "test1.txt");
	//print(RAW_MORPH, "test2.txt");

}
vector<compartment> adjacency_builder::import_raw_morphology() {
	vector<compartment> RES;
	vector<vector<string> > READ;
	vector<int> indx = { 1, 2, 3, 4, 5, 6, 7 };
	READ = readout(path + swc, indx);
	compartment temp;
	vector<double> X(3);

	int ID, par;
	double rad;

	for (unsigned int i = 0; i < READ.size(); i++) {
		X[0] = stod(READ[i][2]);
		X[1] = stod(READ[i][3]);
		X[2] = stod(READ[i][4]);
		ID = stoi(READ[i][1]);
		rad = stod(READ[i][5]);
		par = stoi(READ[i][6]) - 1;
		temp.init(X, ID, rad, par);
		RES.push_back(temp);
	}
	return RES;
}
vector<compartment> adjacency_builder::do_connectivity(vector<compartment> IN) {

	for (unsigned int pz = 0; pz < IN.size(); pz++) {
		IN[pz].C.indx_self = pz;
		IN[pz].C.List_adj.resize(0);
		IN[pz].C.List_post.resize(0);
	}
	IN[0].C.List_adj.push_back(0);

	for (unsigned int pz = 1; pz < IN.size(); pz++) {
		IN[pz].C.List_adj.push_back(IN[pz].C.indx_root);
	}
	for (unsigned int pz = 1; pz < IN.size(); pz++) {
		IN[IN[pz].C.indx_root].C.List_post.push_back(pz);
		IN[IN[pz].C.indx_root].C.List_adj.push_back(pz);
	}
	for (unsigned int pz = 1; pz < IN.size(); pz++) {
		if (IN[pz].C.Num_post == 0) {
			IN[pz].C.List_adj.push_back(pz);
		}
	}


	for (unsigned int pz = 1; pz < IN.size(); pz++) {
		IN[pz].C.Num_post = IN[pz].C.List_post.size();
		IN[pz].C.Num_adj = IN[pz].C.List_adj.size();
	}
	return IN;
}
vector<compartment> adjacency_builder::recenter(vector<compartment> IN) {
	vector<double> X0 = IN[0].G.X;
	for (unsigned int i = 0; i < IN.size(); i++) {
		IN[i].G.X[0] = IN[i].G.X[0] - X0[0];
		IN[i].G.X[1] = IN[i].G.X[1] - X0[1];
		IN[i].G.X[2] = IN[i].G.X[2] - X0[2];
	}
	return IN;
}
double adjacency_builder::d(vector<double> A, vector<double> B) {
	double RES = 0;

	RES = (A[0] - B[0])*(A[0] - B[0]);
	RES += (A[1] - B[1])*(A[1] - B[1]);
	RES += (A[2] - B[2])*(A[2] - B[2]);

	RES = sqrt(RES);
	return RES;
}
vector<compartment> adjacency_builder::calc_distances(vector<compartment> IN) {
	for (unsigned int pz = 1; pz < IN.size(); pz++) {
		IN[pz].G.dist_prec = d(IN[pz].G.X, IN[IN[pz].C.indx_root].G.X);
	}

	IN[0].G.dist_soma = 0;
	for (unsigned int pz = 1; pz < IN.size(); pz++) {
		IN[pz].G.dist_soma = IN[IN[pz].C.indx_root].G.dist_soma + IN[pz].G.dist_prec;
	}

	return IN;
}
adjacency_builder::~adjacency_builder()
{
}
void adjacency_builder::print(vector<compartment> IN, string name) {
	ofstream Ezio(path + name);

	for (int pz = 0; pz<IN.size(); pz++) {
		//ME[pz].plot(Ezio);
		Ezio << pz + 1 << " " << IN[pz].G.ID << " "
			<< IN[pz].G.X[0] << " " << IN[pz].G.X[1] << " " << IN[pz].G.X[2] << " " << 1 << " " << IN[pz].C.indx_root + 1 << endl;
	}
}
void adjacency_builder::print_real_rad(vector<compartment> IN, string name) {
	ofstream Ezio(path + name);

	for (int pz = 0; pz<IN.size(); pz++) {
		//ME[pz].plot(Ezio);
		Ezio << pz + 1 << " " << IN[pz].G.ID << " "
			<< IN[pz].G.X[0] << " " << IN[pz].G.X[1] << " " << IN[pz].G.X[2] << " " << IN[pz].G.radius << " " << IN[pz].C.indx_root + 1 << endl;
	}
}

