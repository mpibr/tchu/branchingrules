#pragma once

 
#include "rule.h"
#include "compartment.h"
#include <vector>
#include <iostream>
#include "teacher.h"
#include <cmath>

#include "pass_structs.h"

using namespace std;

class diff_soluble : public rule
{
public:
	diff_soluble(pass_parameter_SURF_LT &PARAM);
	//diff_soluble(vector<int> LIST, vector<double> Radii, int indx_me, TC rule, double dt);
	//void init(compartment me, TC rule, double dt);

	void give();
	void take(vector<compartment> &GRAPH, int indx);
	void decay_and_create();
	double tell_number() { return N_protein; };
	~diff_soluble() { ; };

	string tell_name() { return ID; }
	



private:
	
	int indx_self;

	double N_protein;
	vector<double> gifts;
	TC my_rule;


	double f;		//fraction decay/creation;
	double f2;


	vector<int> IN;
	vector<int> OUT;
	vector<double> fraction;
	
	void set_IN(compartment &GRAPH);
	void set_FRACTIONS(pass_parameter_SURF_LT &PARAM);
};

