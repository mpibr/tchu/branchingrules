#pragma once
 
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iomanip>
#include <string>

//FOLDER CREATION RELATED
#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
#include <direct.h>
//#elif __APPLE__
#endif



using namespace std;


void MY_SplitString(std::string *full_str, std::string *name, std::vector<std::string> *values);
vector<vector<string> > readout(string name, string trigger, vector<int> indx);
vector<vector<string> > readout(string name, vector<string> trigger1);
vector<vector<string> > readout(string name, vector<int> indx);

void copy_file(string namefile, string wheretocopy, string newname);
double time_converter(string scale, string time);
void create_folder(string name_folder);
