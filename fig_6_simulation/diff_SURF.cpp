#include "diff_SURF.h"
 


diff_SURF::diff_SURF(pass_parameter_SURF_LT &PARAM) {
	//my_rule = PARAM.rules;
	indx_self = PARAM.indx_me;
	//compartment me = GRAPH[indx_me];
	IN = PARAM.List;

	//set_FRACTIONS(Radii, indx_me, dt, rules.dt);
	set_FRACTIONS(PARAM);


	if (PARAM.indx_me == 0) {
		N_protein = 1;
	}
	else {
		N_protein = 0;
	}
	f = 1 - PARAM.GLOBAL_dt / 2 / PARAM.T12;
	f2 = PARAM.GLOBAL_dt / 2 / PARAM.T12;			// fraction to be removed during each time step.
	ID = "DIFF_SURF";
};



void diff_SURF::set_FRACTIONS(pass_parameter_SURF_LT PARAM) {
	double gamma = 1;


	//cout << "FRACITON: MOV " << PARAM.MOVING_fr << endl;
	//cout << "FRACITON:STOP " << PARAM.STOP_fr<< endl;

	//cout << "indx me= "<< indx_self <<"List_size="<< IN.size()<< endl;
	if (indx_self == 0) {
		for (unsigned int i = 0; i < IN.size(); i++) {
			quantifier_out dummy;
			dummy.fraction = pow(PARAM.Radii[i], gamma);
			dummy.indx = PARAM.List[i];
			dummy.N = 0;
			dummy.radius = PARAM.Radii[i];
			Q.push_back(dummy);							//SOMA
		}
		double NORM = 0;
		for (unsigned int i = 0; i < IN.size(); i++) NORM += Q[i].fraction;
		for (unsigned int i = 0; i < IN.size(); i++) Q[i].fraction = Q[i].fraction / NORM;
		for (unsigned int i = 0; i < Q.size(); i++) {
			//cout << "point=" << Q[i].indx << " fraction=" << Q[i].fraction << " N=" << Q[i].N << " rad=" << Q[i].radius << endl;
		}
		for (unsigned int i = 0; i < Q.size(); i++) {
			Q[i].fraction = Q[i].fraction*PARAM.MOVING_fr;
		}
		quantifier_out dummy_self;
		dummy_self.fraction = PARAM.STOP_fr;
		dummy_self.indx = 0;
		dummy_self.N = 0;
		dummy_self.radius = PARAM.rad_self;
		Q.push_back(dummy_self);							//SOMA
	}

	else {
		if (IN.size() == 0) {					//It's a termination
												//cout << "term" << endl;
												//a fraction of particles will move to the root, and some will stay still
			quantifier_out dummy_root;
			quantifier_out dummy_self;

			dummy_root.N = 0;
			dummy_self.N = 0;



			dummy_root.fraction = 0.5*PARAM.MOVING_fr;
			dummy_self.fraction = 0.5*PARAM.MOVING_fr + PARAM.STOP_fr;

			dummy_root.indx = PARAM.indx_root;
			dummy_self.indx = indx_self;

			Q.push_back(dummy_root);
			Q.push_back(dummy_self);
		}
		else if (IN.size() == 1) {				//It's a continuation
												//cout << "cont" << endl;
			quantifier_out dummy_root;
			quantifier_out dummy_self;
			quantifier_out dummy_post;

			dummy_root.N = 0;
			dummy_self.N = 0;
			dummy_post.N = 0;

			dummy_root.fraction = 0.5*PARAM.MOVING_fr;
			dummy_self.fraction = PARAM.STOP_fr;
			dummy_post.fraction = 0.5*PARAM.MOVING_fr;

			dummy_root.indx = PARAM.indx_root;
			dummy_self.indx = indx_self;
			dummy_post.indx = IN[0];

			Q.push_back(dummy_root);
			Q.push_back(dummy_self);
			Q.push_back(dummy_post);
		}
		else if (IN.size() == 2) { 				//It's a bifurcation
			quantifier_out dummy_root;
			quantifier_out dummy_self;
			quantifier_out dummy_post1;
			quantifier_out dummy_post2;


			dummy_root.N = 0;
			dummy_self.N = 0;
			dummy_post1.N = 0;
			dummy_post2.N = 0;


			//FRACTION

			dummy_self.fraction = PARAM.STOP_fr;
			double fr_root = pow(PARAM.rad_self, gamma);
			double fr_post1 = pow(PARAM.Radii[0], gamma);
			double fr_post2 = pow(PARAM.Radii[1], gamma);
			double NORM = fr_root + fr_post1 + fr_post2;
			fr_root = fr_root / NORM;
			fr_post1 = fr_post1 / NORM;
			fr_post2 = fr_post2 / NORM;
			dummy_root.fraction = fr_root * PARAM.MOVING_fr;
			dummy_post1.fraction = fr_post1 * PARAM.MOVING_fr;
			dummy_post2.fraction = fr_post2 * PARAM.MOVING_fr;


			dummy_root.indx = PARAM.indx_root;
			dummy_self.indx = indx_self;
			dummy_post1.indx = IN[0];
			dummy_post2.indx = IN[1];

			Q.push_back(dummy_root);
			Q.push_back(dummy_self);
			Q.push_back(dummy_post1);
			Q.push_back(dummy_post2);
		}
	}

}


void diff_SURF::give() {
	for (unsigned int i = 0; i < Q.size(); i++) {
		Q[i].N = N_protein * Q[i].fraction;
	}
};

void diff_SURF::take(vector<compartment> &GRAPH, int indx) {
	N_protein = 0;
	for (unsigned int i = 0; i < Q.size(); i++) {
		for (unsigned int j = 0; j < GRAPH[Q[i].indx].RULES[0]->Q.size(); j++) {
			if (GRAPH[Q[i].indx].RULES[0]->Q[j].indx == indx) {
				N_protein = N_protein + GRAPH[Q[i].indx].RULES[0]->Q[j].N;
			}
		}
	}
};

void diff_SURF::decay_and_create() {
	N_protein = N_protein * f;
	if (indx_self == 0) {
		N_protein = N_protein + f2;
	}
};