#pragma once
 
	#include <string>
	#include <vector>
	#include "compartment.h"

	using namespace std;

	class compartment;

	struct quantifier_out
	{
		int indx;
		double N;
		double fraction;
		double radius;
	};

	class rule
	{
	public:
		string ID;


		string ID_OUT;
		string ID_IN;
	
		//virtual void init(compartment me,TC rule,double dt) = 0;

		virtual void give() = 0;
		virtual void take(vector<compartment> &GRAPH, int indx) = 0;
		virtual void decay_and_create() = 0;
		virtual double tell_number() = 0;
		virtual string tell_name() = 0;
		vector<quantifier_out> Q;
	
		rule()  { ; };
		~rule() { ; };
	};
