#include "neuron.h"
 
neuron::neuron( simulations param){
	path = param.path;
	swc = param.swc;
	parameter = "parameter.txt";
	//cout << "_A_" << endl;
	adjacency_builder graph(path,swc);
	//cout << "_B_" << endl;
	GRAPH = graph.raw_graph;
	

	dl = graph.dl;
	vector<TC> classes;
	classes=import_cathegories();
	//cout << "_C_" << endl;

	implement_cathegories(classes);

	//graph.teach_rules();


	//GRAPH = build_graph();
}


neuron::~neuron(){

}




vector<TC> neuron::import_cathegories() {
	vector<vector<string> > RULES;
	vector<string> TR;
	TR.push_back("TRANS");
	TR.push_back( "RULE");
	RULES = readout(path+parameter, TR);
	vector<TC> classes;
	dt = 1e300;
	for (unsigned int i = 0; i < RULES.size(); i++) {
		TC dummy;
		dummy.decode(RULES[i], dl);
		classes.push_back(dummy);
		if (dummy.dt < dt) { dt = dummy.dt; }
	}
	return classes;
}

void neuron::implement_cathegories(vector<TC> classes) {

	cout << "I'm here and classes size ==" << classes.size();
	

	for (unsigned int i = 0; i < classes.size(); i++) {
		cout << " i=" << i << classes[i].ID;
		if (!classes[i].ID.compare("DIFF_CYTO")) {
			teach_DIFF_CYTO(dt, classes[i], GRAPH[1].G.dist_prec);
		}
		if (!classes[i].ID.compare("DIFF_SURF")) {
			teach_DIFF_SURF(dt, classes[i], GRAPH[1].G.dist_prec);
		}
		if (!classes[i].ID.compare("DIFF_SURF_LT")) {
			teach_DIFF_SURF_LT(dt, classes[i],GRAPH[1].G.dist_prec);
		}
		if (!classes[i].ID.compare("DIFF_CYTO_LT")) {
			teach_DIFF_CYTO_LT(dt, classes[i], GRAPH[1].G.dist_prec);
		}
	}
	cout << endl;
}


void neuron::teach_DIFF_SURF(double dt,TC rules, double dL) {
	cout << " _____ INSEGNO ____" << endl;
	for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
		pass_parameter_SURF_LT PARAM;

		PARAM.List = GRAPH[pz].C.List_post;

		PARAM.Radii.resize(0);
		PARAM.D = rules.D;
		PARAM.T12 = rules.T12;
		PARAM.fraction = rules.fraction;
		PARAM.GLOBAL_dt = dt;
		PARAM.dL = dL;
		PARAM.MOVING_fr = 2 * PARAM.D*PARAM.GLOBAL_dt / pow(PARAM.dL, 2);
		PARAM.STOP_fr = 1 - 2 * PARAM.D*PARAM.GLOBAL_dt / pow(PARAM.dL, 2);

		PARAM.indx_root = GRAPH[pz].C.indx_root;
		PARAM.rad_self = GRAPH[pz].G.radius;
		PARAM.indx_me = pz;

		for (unsigned int i = 0; i < PARAM.List.size(); i++) {
			PARAM.Radii.push_back(GRAPH[PARAM.List[i]].G.radius);
		}
		GRAPH[pz].RULES.push_back(new diff_SURF(PARAM));
	}
}

void neuron::teach_DIFF_CYTO(double dt, TC rules, double dL) {
	cout << " _____ INSEGNO ____" << endl;
	for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {



		pass_parameter_SURF_LT PARAM;

		PARAM.List = GRAPH[pz].C.List_post;

		PARAM.Radii.resize(0);
		PARAM.D = rules.D;
		PARAM.T12 = rules.T12;
		PARAM.fraction = rules.fraction;
		PARAM.GLOBAL_dt = dt;
		PARAM.dL = dL;
		PARAM.MOVING_fr = 2 * PARAM.D*PARAM.GLOBAL_dt / pow(PARAM.dL, 2);
		PARAM.STOP_fr = 1 - 2 * PARAM.D*PARAM.GLOBAL_dt / pow(PARAM.dL, 2);

		PARAM.indx_root = GRAPH[pz].C.indx_root;
		PARAM.rad_self = GRAPH[pz].G.radius;
		PARAM.indx_me = pz;

		for (unsigned int i = 0; i < PARAM.List.size(); i++) {
			PARAM.Radii.push_back(GRAPH[PARAM.List[i]].G.radius);
		}

		
		GRAPH[pz].RULES.push_back(new diff_soluble(PARAM));
		//cout<<GRAPH[pz].RULES[0]->tell_number() << endl;
	}
}

void neuron::teach_DIFF_SURF_LT(double dt, TC rules,double dL) {

	cout << " _____ INSEGNO ____" << endl;
	for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
		pass_parameter_SURF_LT PARAM;

		PARAM.List = GRAPH[pz].C.List_post;

		
		PARAM.Radii.resize(0);
		PARAM.D = rules.D;
		PARAM.T12 = rules.T12;
		PARAM.fraction = rules.fraction;
		PARAM.GLOBAL_dt = dt;
		PARAM.dL = dL;
		PARAM.MOVING_fr = 2 * PARAM.D*PARAM.GLOBAL_dt / pow(PARAM.dL, 2);
		PARAM.STOP_fr= 1-2 * PARAM.D*PARAM.GLOBAL_dt / pow(PARAM.dL, 2);

		PARAM.indx_root = GRAPH[pz].C.indx_root;
		PARAM.rad_self = GRAPH[pz].G.radius;
		PARAM.indx_me = pz;
		PARAM.VOL = GRAPH[0].G.VOL;
		PARAM.SURF = GRAPH[0].G.VOL;


		for (unsigned int i = 0; i < PARAM.List.size(); i++) {
			PARAM.Radii.push_back(GRAPH[PARAM.List[i]].G.radius);
		}  
		//GRAPH[pz].RULES.push_back(new diff_LP_SURF(LIST, Radii, pz, rules, dt,GRAPH[0].G.VOL,GRAPH[1].G.dist_prec,GRAPH[pz].G.radius));
		GRAPH[pz].RULES.push_back(new diff_LP_SURF(PARAM));
	}
}

void neuron::teach_DIFF_CYTO_LT(double dt, TC rules, double dL) {

	cout << " _____ INSEGNO ____" << endl;
	for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
		pass_parameter_SURF_LT PARAM;

		PARAM.List = GRAPH[pz].C.List_post;


		PARAM.Radii.resize(0);
		PARAM.D = rules.D;
		PARAM.T12 = rules.T12;
		PARAM.fraction = rules.fraction;
		PARAM.GLOBAL_dt = dt;
		PARAM.dL = dL;
		PARAM.MOVING_fr = 2 * PARAM.D*PARAM.GLOBAL_dt / pow(PARAM.dL, 2);
		PARAM.STOP_fr = 1 - 2 * PARAM.D*PARAM.GLOBAL_dt / pow(PARAM.dL, 2);

		PARAM.indx_root = GRAPH[pz].C.indx_root;
		PARAM.rad_self = GRAPH[pz].G.radius;
		PARAM.indx_me = pz;
		PARAM.VOL = GRAPH[0].G.VOL;
		PARAM.SURF = GRAPH[0].G.VOL;


		for (unsigned int i = 0; i < PARAM.List.size(); i++) {
			PARAM.Radii.push_back(GRAPH[PARAM.List[i]].G.radius);
		}
		//GRAPH[pz].RULES.push_back(new diff_LP_SURF(LIST, Radii, pz, rules, dt,GRAPH[0].G.VOL,GRAPH[1].G.dist_prec,GRAPH[pz].G.radius));
		GRAPH[pz].RULES.push_back(new diff_LP_CYTO(PARAM));
	}
}

void neuron::set_temporal_costrain() {
	string trigger = "T_tot";
	vector<int> indx = { 3,4 };
	vector<vector<string> > TIMEINFO = readout(path + parameter, trigger, indx);


	if (TIMEINFO.size() > 0) {
		TT.T_tot = time_converter(TIMEINFO[0][0], TIMEINFO[0][1]);
		TT.CONDITION = "T_TOT";
	}

	trigger = "precision";
	indx = { 3};
	TIMEINFO = readout(path+parameter, trigger, indx);

	if (TIMEINFO.size() > 0) {
		TT.precision = stod(TIMEINFO[0][0]);
		TT.CONDITION = "precision";
	}


	trigger = "T_rec";
	indx = { 3,4 };
	TIMEINFO = readout(path + parameter, trigger, indx);

	if (TIMEINFO.size() > 0) {
		TT.T_print = time_converter(TIMEINFO[0][0], TIMEINFO[0][1]);
		TT.N_print = max(1.0, round(TT.T_print / dt));
	}
	else {
		if (!TT.CONDITION.compare("T_TOT")) {
			TT.T_print = TT.T_tot / 10;
		}
		else {
			TT.T_print = 10 * 3600;			//10 hours
			TT.N_print = max(1.0, round(TT.T_print / dt));
		}
	}
}

void neuron::evolve() {

	cout << "EVOLVE" << endl;


	set_temporal_costrain();


	long unsigned int it = 0;
	int counter = 0;
	bool flag = 1;


	double Nn = 0;

	double MM = -2;

	vector<double> OLD;
	vector<double> NEW;
	vector<double> DIFF;

	if (!TT.CONDITION.compare("precision")) {
		OLD.resize(GRAPH.size(), 0);
		NEW.resize(GRAPH.size(), 0);
		DIFF.resize(GRAPH.size(), 0);

		for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
			OLD[pz] = -200000;
		}
	}
	

	while (flag==1){
		if (it%TT.N_print == 0) {

			string name= "DEND=" + to_string(counter) + ".txt";
			counter++;
			//cout << "STEP " << it << "	t=" << 1.0* it * dt / 3600 << " [h]	-- NM= " << count_mol_continue(PAR, 0) << "	-- %err:  ";
			
			double NNN = 0;
			for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
				NNN = NNN + GRAPH[pz].RULES[0]->tell_number();
			}
			if (!TT.CONDITION.compare("precision")) {

				MM = -2;
				for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
					NEW[pz] = GRAPH[pz].RULES[0]->tell_number();
					DIFF[pz] = abs(NEW[pz] - OLD[pz]);
					OLD[pz] = NEW[pz];
				}

				for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
					if (DIFF[pz] > MM) { MM = DIFF[pz]; }
				}


			}
			cout << "STEP " << it << "	t=" << 1.0* it * dt / 3600 << " [h] N=" << NNN;
			if (!TT.CONDITION.compare("precision")) { cout << " precision=" << MM; }
			cout<< endl;
			print(GRAPH, name);



			flag = condition(it,MM);

			//int a; cin >> a;

		}

		give_all();
		take_all();
		decay_and_create_all();
		it++;
	}
}

void neuron::give_all() {
	for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
		for (unsigned int NR = 0; NR < GRAPH[pz].RULES.size(); NR++) {
			GRAPH[pz].RULES[NR]->give();
		}
	}
}
void neuron::take_all() {
	for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
		for (unsigned int NR = 0; NR < GRAPH[pz].RULES.size(); NR++) {
			GRAPH[pz].RULES[NR]->take(GRAPH,pz);
		}
	}
}
void neuron::decay_and_create_all() {
	for (unsigned int pz = 0; pz < GRAPH.size(); pz++) {
		for (unsigned int NR = 0; NR < GRAPH[pz].RULES.size(); NR++) {
			GRAPH[pz].RULES[NR]->decay_and_create();
		}
	}
}











 


bool neuron::condition(long unsigned int i,double MM) {

	if (!TT.CONDITION.compare("precision")) {
		//cout << "check precision " << MM << endl;
		if (MM > TT.precision) return 1;
		return 0;
	}
	if (!TT.CONDITION.compare("T_TOT")) {
		if (i*dt < TT.T_tot) return 1;
		return 0;
	}
}
void neuron::print(vector<compartment> IN, string name) {
	
	ofstream Ezio(path + name);

	for (int pz = 0; pz<IN.size(); pz++) {
		//ME[pz].plot(Ezio);
		Ezio << IN[pz].G.X[0] << " " << IN[pz].G.X[1] << " " << IN[pz].G.X[2] << " " << IN[pz].G.dist_soma << " ";
		Ezio << IN[pz].G.ID << " " << IN[pz].G.radius << " ";
		Ezio << IN[pz].RULES[0]->tell_number() << endl;
	}
}

  
