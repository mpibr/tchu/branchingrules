#include "adjacency_builder.h"

 
vector<compartment> adjacency_builder::cut(vector<compartment> IN) {
	vector<vector<string> > WHOTOCUT;
	vector<vector<string> > CUTSOMA;

	vector<int> indx = { 2 };
	vector<int> indx2 = { 2 };

	WHOTOCUT = readout(path + parameter, "GRAPH_REDUCTION", indx);
	CUTSOMA = readout(path + parameter, "SOMA_CONTRACTION", indx);
	if (WHOTOCUT.size() > 0) {
		for (int i = 0; i < WHOTOCUT.size(); i++) {
			IN = remove_ID(IN, stoi(WHOTOCUT[i][0]));
		}
	}
	if (CUTSOMA.size() > 0) {
		if (!CUTSOMA[0][0].compare("YES")) {
			IN = shrink_soma(IN);
		}
	}
	IN = do_connectivity(IN);
	return IN;
}
void adjacency_builder::remove_single_piece(vector<compartment> &IN, int me_pz) {

	for (unsigned int pz = me_pz + 1; pz < IN.size(); pz++) {
		//change the root if it's pointing to me_pz or bigger.
		if (IN[pz].C.indx_root == me_pz) {
			IN[pz].C.indx_root = IN[IN[pz].C.indx_root].C.indx_root;
		}
		else if (IN[pz].C.indx_root > me_pz) { IN[pz].C.indx_root--; }
	}
	vector<int> indx = { me_pz };
	IN.erase(IN.begin() + me_pz);
}
vector<compartment> adjacency_builder::remove_ID(vector<compartment> IN, int REMOVEME) {
	int count = 0;
	for (int pz = 1; pz < IN.size();) {
		if (IN[pz].G.ID == REMOVEME) {
			remove_single_piece(IN, pz);
			count++;
		}
		else {
			pz++;
		}
	}
	return IN;
}
vector<compartment> adjacency_builder::shrink_soma(vector<compartment> IN) {
	IN = remove_ID(IN, 1);
	return IN;
}



