#include "adjacency_builder.h"
 

vector<compartment> adjacency_builder::regularize_graph(vector<compartment> to_simplify) {
	//the goal of this function is to fix the length of each compartment to DX

	string trigger = "min_dL";
	vector<int> indx = { 3 };
	vector<vector<string> > DDL = readout(path + parameter, trigger, indx);
	dl = stod(DDL[0][0]);
	to_simplify = join(dl, to_simplify);
	to_simplify = split(dl, to_simplify);
	return to_simplify;
};
vector<double> adjacency_builder::calc_shift(vector<double> Pre, vector<double>  Post, double D) {

	vector<double> shift(3, 0);
	double current_distance = d(Pre, Post);

	double x1, x2, y1, y2, z1, z2;

	x2 = Post[0] - Pre[0];
	y2 = Post[1] - Pre[1];
	z2 = Post[2] - Pre[2];

	double x22 = pow(x2, 2);
	double y22 = pow(y2, 2);
	double z22 = pow(z2, 2);

	double DX, DY, DZ;

	DX = D * x2 / sqrt(x22 + y22 + z22);
	DY = D * y2 / sqrt(x22 + y22 + z22);
	DZ = D * z2 / sqrt(x22 + y22 + z22);

	vector<double> dummyP;
	dummyP = Pre;
	Pre[0] = Pre[0] + DX;
	Pre[1] = Pre[1] + DY;
	Pre[2] = Pre[2] + DZ;

	vector<double> dummyM;
	dummyM = Pre;
	Pre[0] = Pre[0] - DX;
	Pre[1] = Pre[1] - DY;
	Pre[2] = Pre[2] - DZ;


	double DistM;
	DistM = d(dummyM, Pre);
	double DistP;
	DistP = d(dummyP, Pre);
	if (DistM < DistP) {
		shift[0] = -DX;
		shift[1] = -DX;
		shift[2] = -DX;
	}
	else {
		shift[0] = DX;
		shift[1] = DY;
		shift[2] = DZ;
	}

	return shift;
}
vector<compartment> adjacency_builder::join(double DL, vector<compartment> OLD) {
	vector<compartment> NEW;
	NEW.resize(0);

	OLD[0].C.indx_root = -1;


	for (int i = 1; i < OLD.size(); i++) {


		OLD[i].G.dist_prec = d(OLD[i].G.X, OLD[OLD[i].C.indx_root].G.X);
		//cout <<"DISTANCE OF"<< i << " FROM ITS ROOT " << OLD[i].G.distance_from_the_previous << endl;
		if (OLD[i].G.dist_prec < DL) {

			if (OLD[i].C.Num_post == 0) {
				//cout << "join NP0" << endl;

				int j = OLD[i].C.indx_root;
				vector<double> shift;

				if (d(OLD[j].G.X, OLD[i].G.X) < 0.001) {
					shift.resize(3, 0);
					//i = i - 1;			//NOT SURE. Should I put it? let's see
				}
				else {
					shift = calc_shift(OLD[j].G.X, OLD[i].G.X, DL);
				}
				//cout << "NP=0	" << i << " " << shift[0] << " " << shift[1] << " " << shift[2] << endl;
				//int a; cin >> a;
				move(OLD[i], OLD[j], shift);



			}
			if (OLD[i].C.Num_post == 1) {
				//cout << "join NP1" << endl;

				int j = OLD[i].C.indx_root;
				//cout << "crash here? 1" << endl;
				//check if merging two points will increase or decrease my precision. if |DL-d(j,i+1)|<|DL-d(j,i)| will do.
				if (abs(DL - d(OLD[j].G.X, OLD[i + 1].G.X)) < abs(DL - d(OLD[j].G.X, OLD[i].G.X))) {
					OLD[i + 1].C.indx_root = j;
					OLD[i + 1].G.dist_prec = d(OLD[j].G.X, OLD[i + 1].G.X);

					for (int k = i + 1; k < OLD.size(); k++) {
						if (OLD[k].C.indx_root > i) {
							OLD[k].C.indx_root = OLD[k].C.indx_root - 1;
						}
					}
					OLD.erase(OLD.begin() + i);
					i = i - 1;
				}
			}
			if (OLD[i].C.Num_post == 2 && i>0) {
				//cout << "join NP2" << endl;

				int j = OLD[i].C.indx_root;
				vector<double> shift;
				if (d(OLD[j].G.X, OLD[i].G.X) < 0.001) {
					shift.resize(3, 0);
					//i = i - 1;			//NOT SURE. Should I put it? let's see
				}
				else {
					OLD[i].G.dist_prec = d(OLD[i].G.X, OLD[OLD[i].C.indx_root].G.X);
					double dl = DL - OLD[i].G.dist_prec;
					shift = calc_shift(OLD[j].G.X, OLD[i].G.X, dl);

				}

				//cout << "NP=2	" << i << " " << shift[0] << " " << shift[1] << " " << shift[2] << endl;
				//int a; cin >> a;

				vector<int> list_movendi;
				list_movendi = findpost(i, OLD);
				for (int k = 0; k < list_movendi.size(); k++) {
					move(OLD[list_movendi[k]], shift);
				}
			}

			for (int pz = 0; pz < OLD.size(); pz++) {
				OLD[pz].C.List_post.resize(0);
			}
			for (int pz = 1; pz < OLD.size(); pz++) {
				int indx = OLD[pz].C.indx_root;
				OLD[indx].C.List_post.push_back(pz);
			}
		}
	}

	for (int pz = 0; pz < OLD.size(); pz++) {
		OLD[pz].C.List_post.resize(0);
	}
	for (int pz = 1; pz < OLD.size(); pz++) {
		int indx = OLD[pz].C.indx_root;
		OLD[indx].C.List_post.push_back(pz);
	}
	for (int pz = 1; pz < OLD.size(); pz++) {
		OLD[pz].C.Num_adj = OLD[pz].C.List_adj.size();
		OLD[pz].C.Num_post = OLD[pz].C.List_post.size();
	}

	return OLD;
};
vector<int> adjacency_builder::findpost(int indx, vector<compartment> OLD) {
	int L = OLD.size();
	vector<int> FLAG(L, 0);
	FLAG[indx] = 1;
	for (int i = indx; i < L; i++) {
		int j = OLD[i].C.indx_root;
		if (FLAG[j] == 1) {
			FLAG[i] = 1;
		}
	}
	vector<int> RES;
	for (int i = 0; i < L; i++) {
		if (FLAG[i] == 1) {
			RES.push_back(i);
		}
	}
	return RES;
}
vector<compartment> adjacency_builder::split(double DL, vector<compartment> OLD) {
	vector<compartment> BU;
	BU = OLD;
	int j;
	for (int i = 1; i < OLD.size(); i++) {
		j = OLD[i].C.indx_root;
		if (d(OLD[i].G.X, OLD[j].G.X) > 1.5*DL) {

			compartment dummy = OLD[i];
			vector<double> shift;
			shift = calc_shift(OLD[j].G.X, OLD[i].G.X, DL);

			dummy.G.X[0] = OLD[j].G.X[0] + shift[0];
			dummy.G.X[1] = OLD[j].G.X[1] + shift[1];
			dummy.G.X[2] = OLD[j].G.X[2] + shift[2];
			dummy.G.dist_prec = d(dummy.G.X, OLD[j].G.X);



			for (int pz = 0; pz < OLD.size(); pz++) {
				OLD[pz].C.List_post.resize(0);
			}
			for (int pz = 1; pz < OLD.size(); pz++) {
				int indx = OLD[pz].C.indx_root;
				OLD[indx].C.List_post.push_back(pz);
			}

			OLD.insert(OLD.begin() + i, dummy);

			//Everything after i will now point at i+1
			for (int k = 0; k < OLD.size(); k++) {
				if (OLD[k].C.indx_root >= i) {
					OLD[k].C.indx_root = OLD[k].C.indx_root + 1;
				}
			}

			OLD[i + 1].C.indx_root = i;

			for (int pz = 0; pz < OLD.size(); pz++) {
				OLD[pz].C.List_post.resize(0);
			}
			for (int pz = 1; pz < OLD.size(); pz++) {
				int indx = OLD[pz].C.indx_root;
				OLD[indx].C.List_post.push_back(pz);
			}
			for (int pz = 1; pz < OLD.size(); pz++) {
				if (OLD[pz].C.List_post.size() > 2) {
					cout << "THE ERROR IS HERE! pz=" << pz << " during i= " << i << endl;
					for (int kk = 0; kk < OLD[pz].C.List_post.size(); kk++) {
						cout << OLD[pz].C.List_post[kk] << " ";
					}cout << endl;
					int aa; cin >> aa;
				}
			}

		}
	}
	return OLD;
};
void adjacency_builder::move(compartment &ME, vector<double> shift) {
	ME.G.X[0] = ME.G.X[0] + shift[0];
	ME.G.X[1] = ME.G.X[1] + shift[1];
	ME.G.X[2] = ME.G.X[2] + shift[2];
}
void adjacency_builder::move(compartment &ME, compartment &Refrence, vector<double> shift) {
	ME.G.X[0] = Refrence.G.X[0] + shift[0];
	ME.G.X[1] = Refrence.G.X[1] + shift[1];
	ME.G.X[2] = Refrence.G.X[2] + shift[2];
}





