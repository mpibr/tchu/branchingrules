#include "teacher.h"
#include "utilities.h"

 
void TC::decode(vector<string> RULES,double dx) {
	if (!RULES.at(0).compare("TRANS")) {

		//	TRANS	ID	DIFFUSION_RADIUS_INSIDE NAME	D	[um^2/sec]	#	tau_1/2		[h]		#
		//	0		1	2						3		4	5			6	7			8		9
		if (!RULES.at(2).compare("DIFFUSION_RADIUS_INSIDE")) {
			ID = "DIFF_CYTO";
			T12 = time_converter(RULES.at(8), RULES.at(9));
			D = stod(RULES.at(6));
			dt = 0.5 * pow(dx,2) / D;
		}
		
		//	TRANS	ID	DIFFUSION_RADIUS_SURFACE NAME	D	[um^2/sec]	#	tau_1/2		[h]		#
		//	0		1	2						 3		4	5			6	7			8		9
		else if (!RULES.at(2).compare("DIFFUSION_RADIUS_SURFACE")) {
			ID = "DIFF_SURF";
			T12 = time_converter(RULES.at(8), RULES.at(9));
			D = stod(RULES.at(6));
			dt = 0.5 * pow(dx, 2) / D;
		}

		//	TRANS	ID	DIFFUSION_SURFACE_LT NAME	D	[um^2/sec]	#	tau_1/2		[h]		#	fraction=	#
		//	0		1	2					 3		4	5			6	7			8		9	10			11
		else if (!RULES.at(2).compare("DIFF_SURF_LT")) {
			ID = "DIFF_SURF_LT";
			T12 = time_converter(RULES.at(8), RULES.at(9));
			D = stod(RULES.at(6));
			dt = 0.5 * pow(dx, 2) / D;
		}

		//	TRANS	ID	DIFFUSION_CYTO_LT	NAME	D	[um^2/sec]	#	tau_1/2		[h]		#	fraction=	#
		//	0		1	2					3		4	5			6	7			8		9	10			11
		else if (!RULES.at(2).compare("DIFF_CYTO_LT")) {
			ID = "DIFF_CYTO_LT";
			T12 = time_converter(RULES.at(8), RULES.at(9));
			D = stod(RULES.at(6));
			dt = 0.5 * pow(dx, 2) / D;
		}
		else { cout << "NOT IMPLEMENTED YET" << endl; }
	}
	else if (!RULES.at(0).compare("RULE")) {

		//	RULE	DIFF_CYTO	D=	#	[um2/s]		T12=	#	[min/sec/h/day/..]
		//	0		1			2	3	4			5		6	7
		if (!RULES.at(1).compare("DIFF_CYTO")) {
			ID = "DIFF_CYTO";
			T12 = time_converter(RULES.at(7), RULES.at(6));
			D = stod(RULES.at(3));
			dt = 0.5 * pow(dx, 2) / D;
		}
		//	RULE	DIFF_CYTO	D=	#	[um2/s]		T12=	#	[min/sec/h/day/..]
		//	0		1			2	3	4			5		6	7
		else if (!RULES.at(1).compare("DIFF_SURF")) {
			ID = "DIFF_SURF";
			T12 = time_converter(RULES.at(7), RULES.at(6));
			D = stod(RULES.at(3));
			dt = 0.5 * pow(dx, 2) / D;
		}
		//	RULE	DIFFUSION_SURF_LT	D=	#	[um2/s]		T12=	#	[min/sec/h/day/..]	fr_dend=	#
		//	0		1					2	3	4			5		6	7					8			9
		else if (!RULES.at(1).compare("DIFF_SURF_LT")) {

			ID = "DIFF_SURF_LT";
			T12 = time_converter(RULES.at(7), RULES.at(6));
			D = stod(RULES.at(3));
			dt = 0.5 * pow(dx, 2) / D;
			fraction = stod(RULES.at(9));
		}
		//	RULE	DIFFUSION_CYTO_LT	D=	#	[um2/s]		T12=	#	[min/sec/h/day/..]	fr_dend=	#
		//	0		1					2	3	4			5		6	7					8			9
		else if (!RULES.at(1).compare("DIFF_CYTO_LT")) {
			ID = "DIFF_CYTO_LT";
			T12 = time_converter(RULES.at(6), RULES.at(6));
			D = stod(RULES.at(3));
			dt = 0.5 * pow(dx, 2) / D;
			fraction = stod(RULES.at(9));
		}
		else { cout << "NOT IMPLEMENTED YET" << endl; }
	}
	cout << ID << endl;
}
