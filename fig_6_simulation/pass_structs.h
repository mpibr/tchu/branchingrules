#pragma once
 
#include <vector>

using namespace std;



struct pass_parameter_SURF_LT {
	vector<double> Radii;
	vector<int> List;
	double GLOBAL_dt;
	double my_dt;

	double dist_prec;
	double fraction;
	double T12;
	double D;
	double dL;
	int indx_me;
	int indx_root;
	double rad_self;


	double MOVING_fr;
	double STOP_fr;

	double VOL;
	double SURF;
};


