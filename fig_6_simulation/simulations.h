#pragma once
 
	#include <string>
	#include <vector>
	#include <fstream>
	#include <iostream>
	#include "utilities.h"



	using namespace std;

	class simulations
	{
	public:


		string path;
		string swc;

		void init_SYMM(vector<string> INFO,string pre_path, string oldparametername);
		void init_SWC(vector<string> INFO,string pre_path, string oldparametername);




		simulations();
		~simulations();
	private:
	
		string WHO;
		void create_symm_swc(string path, string swc, vector<string> INFO);
	
	
		int N;		//Number of branches in case of Synthetic neurons
		double L;	//Distance between branches in case of Synth. Neurons.

	};

	vector<vector<string>> read_List_parameters();
	vector<simulations> init_simulations();


	struct element {
		vector<double> X;
		int root;
		int generation;
		double theta;
	};


	class ARTIFICIAL_GRAPH {
	public:
	
		vector<element> piece;
	
		void init(int N, double L, string path, string swc);
		void print();
	private:
		void add_kayley_banch(double theta);
		vector<double> R(double theta, vector<double> X);
		int N;
		double L;
		string path;
		string swc;
	
	};

