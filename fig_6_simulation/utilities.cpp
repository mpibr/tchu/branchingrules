#include "utilities.h"

 


void MY_SplitString(std::string *full_str, std::string *name, std::vector<std::string> *values) {

	std::replace(full_str->begin(), full_str->end(), '\t', ' '); //Replace tabs by spaces, then search for tabs
	std::replace(full_str->begin(), full_str->end(), '\r', ' ');  //Replace additional end-of-line \r-symbol

	std::istringstream iss(*full_str);
	std::string        token;
	values->clear();
	while (std::getline(iss, token, ' ')) {
		if (token.length() != 0) {
			values->push_back(token);
		}
	}
}



vector<vector<string> > readout(string filename, string trigger, vector<int> indx) {
	vector<vector<string> > RES;

	for (int i = 0; i < indx.size(); i++) {
		indx[i] = indx[i] - 1;
	}

	char                     line[16384];
	std::vector<std::string> full_strs, values;
	std::string              str_line, name;

	string dummy;
	ifstream Ezio(filename);
	if (!Ezio.is_open()) {
		cout << "WARNING!! " << filename << " NOT FOUND!" << endl;
		int a;
		cin >> a;
	}
	else {
		while (Ezio.getline(line, 16384)) {
			if (line[0] == '#') {
				continue;
			}
			full_strs.push_back(line);
			str_line = line;
			MY_SplitString(&str_line, &name, &values);

			bool flag = 0;
			for (unsigned int i = 0; i < values.size(); i++) {
				if (!trigger.compare(values.at(i))) flag = 1;
			}

			if (flag == 1) {
				vector<string> partial;
				for (int i = 0; i < indx.size(); i++) {
					partial.push_back(values.at(indx[i]));
				}
				RES.push_back(partial);
			}
		}
	}
	return RES;
}

vector<vector<string> > readout(string filename, vector<string> trigger) {
	vector<vector<string> > RES;


	char                     line[16384];
	std::vector<std::string> full_strs, values;
	std::string              str_line, name;

	string dummy;
	ifstream Ezio(filename);
	if (!Ezio.is_open()) {
		cout << "WARNING!! " << filename << " NOT FOUND!" << endl;
		int a;
		cin >> a;
	}
	else {
		while (Ezio.getline(line, 16384)) {
			if (line[0] == '#') {
				continue;
			}
			full_strs.push_back(line);
			str_line = line;
			MY_SplitString(&str_line, &name, &values);

			bool flag = 0;
			for (unsigned int i = 0; i < values.size(); i++) {
				for (unsigned int j = 0; j < trigger.size(); j++) {
					if (!trigger.at(j).compare(values.at(i))) flag = 1;
				}
			}

			if (flag == 1) {
				vector<string> partial;
				for (int i = 0; i < values.size(); i++) {
					partial.push_back(values.at(i));
				}
				RES.push_back(partial);
			}
		}
	}
	return RES;
}



vector<vector<string> > readout(string filename,  vector<int> indx) {
	vector<vector<string> > RES;

	for (int i = 0; i < indx.size(); i++) { 
		indx[i] = indx[i] - 1;
	}

	char                     line[16384];
	std::vector<std::string> full_strs, values;
	std::string              str_line, name;

	string dummy;
	ifstream Ezio(filename);
	if (!Ezio.is_open()) {
		cout << "WARNING!! " << filename << " NOT FOUND!" << endl;
		int a;
		cin >> a;
	}
	else {
		while (Ezio.getline(line, 16384)) {
			if (line[0] == '#') {
				continue;
			}
			full_strs.push_back(line);
			str_line = line;
			MY_SplitString(&str_line, &name, &values);

			bool flag = 1;
			if (flag == 1) {
				vector<string> partial;
				for (int i = 0; i < indx.size(); i++) {
					partial.push_back(values.at(indx[i]));
				}
				RES.push_back(partial);
			}
		}
	}
	return RES;
}

void copy_file(string namefile, string wheretocopy, string newname) {
	
	char	line[16384];
	string	str;

	ifstream COPY(namefile);									// load from local
	ofstream PASTE(wheretocopy + newname);				// save tree in proper folder

	if (COPY.is_open()) { ; }
	else {
		cout << "THERE IS NO " <<namefile<< " TO COPY! " << endl;
	}
	if (PASTE.is_open()) { ; }
	else {
		cout << "FAILED TO CREATE copy of" << namefile << endl;
	}

	while (!COPY.eof()) {
		COPY.getline(line, 16384);
		str = line;
		if (str.size() > 1) {
			PASTE << str << endl;
		}
	}
	COPY.close();
	PASTE.close();
}



double time_converter(string scale, string time) {

	//double inf = 1.0 / 0.0;

	if (!scale.compare("[s]"))	return (stod(time));
	if (!scale.compare("[S]"))	return (stod(time));
	else if (!scale.compare("[m]"))	return (stod(time) * 60);
	else if (!scale.compare("[M]"))	return (stod(time) * 60);
	else if (!scale.compare("[h]"))	return (stod(time) * 60 * 60);
	else if (!scale.compare("[H]"))	return (stod(time) * 60 * 60);
	else if (!scale.compare("[d]"))	return (stod(time) * 60 * 60 * 24);
	else if (!scale.compare("[D]"))	return (stod(time) * 60 * 60 * 24);
	else if (!scale.compare("[w]"))	return (stod(time) * 60 * 60 * 24 * 7);
	else if (!scale.compare("[W]"))	return (stod(time) * 60 * 60 * 24 * 7);

	else {
		cout << "TIME CONVERSION FAILED! I Don't know how to handle " << scale << endl;
		int a;
		cin >> a;
		return -1;
	}
}


void create_folder(string name_folder) {
	name_folder = name_folder;

#ifdef _WIN32
	_mkdir(name_folder.c_str());
#elif __APPLE__
	mkdir(name_folder.c_str(), 0744);
#elif __linux__
	mkdir(name_folder.c_str(), 0744);
#endif
}
