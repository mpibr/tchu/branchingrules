#pragma once
 

	#include <string>
	#include <fstream>
	#include <vector>
	#include <iostream>
	#include "compartment.h"
	#include "utilities.h"


	using namespace std;

	struct bifurcations{
		int indx_ME;
		int indx_Mother;
		int indx_Daughter1;
		int first_Daughter_1;
		int indx_Daughter2;
		int first_Daughter_2;

		double L0;
		double L1;
		double L2;
	
		double r1;
		double r2;
	
		int generation;

	};



	class adjacency_builder
	{
	public:
		//vector<compartment> EXPO() { ; };

		adjacency_builder(string path,string swc);
		~adjacency_builder();

		void print_real_rad(vector<compartment> IN, string name);
		void print(vector<compartment> IN, string name);

		vector<compartment> raw_graph;
		double dl;
		double Rall_Power;
	private:

	
		vector<compartment> import_raw_morphology();

		vector<compartment> recenter(vector<compartment> IN);
	
		vector<compartment> do_connectivity(vector<compartment> IN);
	
		vector<compartment> calc_distances(vector<compartment> IN);
			double d(vector<double> A, vector<double> B);
	
		vector<compartment> cut(vector<compartment> IN);
			vector<compartment> remove_ID(vector<compartment> IN, int REMOVEME);
				void remove_single_piece(vector<compartment> &Graph, int me_pz);
			vector<compartment> shrink_soma(vector<compartment> IN);
	
		vector<compartment> regularize_graph(vector<compartment> IN);
			vector<compartment> join(double dl, vector<compartment> to_simplify);
			vector<compartment> split(double dl, vector<compartment> to_simplify);
			vector<double> calc_shift(vector<double> Pre, vector<double>  Post, double D);

			vector<int> findpost(int indx, vector<compartment> OLD);
			void move(compartment &ME, vector<double> shift);
			void move(compartment &ME, compartment &Refrence, vector<double> shift);


		vector<compartment> impose_radii(vector<compartment> IN);
			vector<compartment> init_SYMM(vector<compartment> names);
			vector<compartment> init_SYMM_PYR(vector<compartment> name);
			vector<compartment> init_OPT(vector<compartment> name, int gamma);
			

			vector<bifurcations> build_backbone(vector<compartment> IN);
				vector<bifurcations> optimize_backbone(vector<bifurcations> BIF, int gamma);
				vector<compartment> implement_radii(vector<compartment> IN, vector<bifurcations> BIF);
					vector<double> calc_optimal(bifurcations BIF, int gamma);
					double optimal_r1(double lambda, double L1, double L2, int gamma);

					double effective_L(double lambda, double L0, double L1, double L2, double r1, double r2, double gamma);



					void import_lambda();
		
					vector<compartment> pyr(vector<compartment> IN, double R_A, double R_B);
					void calc_vol(vector<compartment> &IN);
		string path;
		string parameter;
		string swc;

		double lambda;
		double lambdaS;
		double lambdaC;		//CYTO

	};
