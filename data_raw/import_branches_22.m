function [ OUTCOME ] = import_branches_22( ROF,FOL )

counter=1;

for FFF=1:length(FOL)
    %ROF
    %pause()
    
    cd(['./' num2str(FOL(FFF))])

    LIST_TRIPLET=importdata('LIST.txt');
    BRANCHES=[];


    Ntriplet=size(LIST_TRIPLET,1);
    cd('./fluo')
    for i=1:max(max(LIST_TRIPLET))
        name=[num2str(i-1) '.csv'];
        dummy=importdata(name, ',',1);
        ALLDATA(i).channel1=dummy.data;
    end
    cd('./../fluo_BG')
    for i=1:max(max(LIST_TRIPLET))
        name=[num2str(i-1) '.csv'];
        dummy=importdata(name, ',',1);
        ALLDATA(i).channel1_BG=dummy.data;
    end
    
    for i=1:Ntriplet
        RES(counter).M__1=ALLDATA(LIST_TRIPLET(i,1)).channel1;
        RES(counter).D1_1=ALLDATA(LIST_TRIPLET(i,2)).channel1;
        RES(counter).D2_1=ALLDATA(LIST_TRIPLET(i,3)).channel1;
        
        RES(counter).M__1_BG=ALLDATA(LIST_TRIPLET(i,1)).channel1_BG;
        RES(counter).D1_1_BG=ALLDATA(LIST_TRIPLET(i,2)).channel1_BG;
        RES(counter).D2_1_BG=ALLDATA(LIST_TRIPLET(i,3)).channel1_BG;
	
        RES(counter).L0=max(RES(counter).M__1(:,1));
        RES(counter).L1=max(RES(counter).D1_1(:,1));
        RES(counter).L2=max(RES(counter).D2_1(:,1));
        
        RES(counter).L0_BG=max(RES(counter).M__1_BG(:,1));
        RES(counter).L1_BG=max(RES(counter).D1_1_BG(:,1));
        RES(counter).L2_BG=max(RES(counter).D2_1_BG(:,1));
        RES(counter).N_fig=FFF;
        RES(counter).indx=i;
        
        counter=counter+1;

   end
    cd ('./../..')

end


VEC0M=[];
VEC1M=[];
VEC2M=[];

VEC0M_BG=[];
VEC1M_BG=[];
VEC2M_BG=[];
INDX=[];
N_fig=[];
R0=[];
R1=[];
R2=[];


for i=1:length(RES)
    minl=min(min(RES(i).L0,RES(i).L1),RES(i).L2);
    minlbg=min(min(RES(i).L0_BG,RES(i).L1_BG),RES(i).L2_BG);
    if(min(minl,minlbg)>ROF(3))
        RES(i).M__1=flipMAT(RES(i).M__1);
        
        RES(i).M__1_BG=flipMAT(RES(i).M__1_BG);
        
        
        RES(i).M__1_cut=cut(RES(i).M__1,ROF);
        RES(i).D1_1_cut=cut(RES(i).D1_1,ROF);
        RES(i).D2_1_cut=cut(RES(i).D2_1,ROF);

        RES(i).M__1_cut_BG=cut(RES(i).M__1_BG,ROF);
        RES(i).D1_1_cut_BG=cut(RES(i).D1_1_BG,ROF);
        RES(i).D2_1_cut_BG=cut(RES(i).D2_1_BG,ROF);

        
        RES(i).FLAG=1;
        
        VEC0M=[VEC0M,mean(RES(i).M__1_cut(:,2))];
        VEC1M=[VEC1M,mean(RES(i).D1_1_cut(:,2))];
        VEC2M=[VEC2M,mean(RES(i).D2_1_cut(:,2))];
        
        VEC0M_BG=[VEC0M_BG,mean(RES(i).M__1_cut_BG(:,2))];
        VEC1M_BG=[VEC1M_BG,mean(RES(i).D1_1_cut_BG(:,2))];
        VEC2M_BG=[VEC2M_BG,mean(RES(i).D2_1_cut_BG(:,2))];
        INDX=[INDX,RES(i).indx];
        N_fig=[N_fig,RES(i).N_fig];
        
    else
        RES(i).FLAG=0;
    end
end


OUTCOME.R0=R0;
OUTCOME.R1=R1;
OUTCOME.R2=R2;


OUTCOME.V01=VEC0M;
OUTCOME.V11=VEC1M;
OUTCOME.V21=VEC2M;


OUTCOME.V01_BG=VEC0M_BG;
OUTCOME.V11_BG=VEC1M_BG;
OUTCOME.V21_BG=VEC2M_BG;


OUTCOME.indx=INDX;
OUTCOME.N_fig=N_fig;






end

