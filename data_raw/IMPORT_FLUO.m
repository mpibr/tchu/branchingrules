function IMPORT_FLUO(path)

    if(exist('all_fluo.mat')==2)

    else
        if(exist(path)==0)
            path='C:\Users\sartorif\Desktop\Fluorescence\';
        end
        path=[path '\'];
        there=pwd;
        addpath(there)
        cd(path);
        
        
        
        
        mm=[0 0 0 0 1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4];
        DD=[1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];
        
        
        
        for i=1:length(mm)
            disp([num2str(i) 'of ' num2str(length(mm))])
            ROF=[mm(i) DD(i) mm(i)+DD(i)];
            RES(i)=parse_single_ROF(ROF,path);
        end
        cd(there)
        rmpath(there)
        
        save all_fluo;
        
        clear all
    end
    load('all_fluo.mat');


end