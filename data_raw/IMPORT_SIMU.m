function IMPORT_SIMU(path)

    if(exist('all_simu_data.mat')==2)

    else
        here=pwd;
        addpath(here);
        cd(path);
        
        

        [X_A_0_C,Y_A_0_C]=import_all_simu_FOLD([path '\410\artificial\G0\CYTO']);
        [X_A_2_C,Y_A_2_C]=import_all_simu_FOLD([path '\410\artificial\G2\CYTO']);
        [X_A_8_C,Y_A_8_C]=import_all_simu_FOLD([path '\410\artificial\G8\CYTO']);

        [X_A_0_S,Y_A_0_S]=import_all_simu_FOLD([path '\410\artificial\G0\SURF']);
        [X_A_2_S,Y_A_2_S]=import_all_simu_FOLD([path '\410\artificial\G2\SURF']);
        [X_A_8_S,Y_A_8_S]=import_all_simu_FOLD([path '\410\artificial\G8\SURF']);

        [X_Sym_C,Y_Sym_C,ALL_S_C,TREE_S_C]=import_all_simu_FOLD([path '\410\symm\CYTO']);
        [X_Sym_S,Y_Sym_S,ALL_S_S,TREE_S_S]=import_all_simu_FOLD([path '\410\symm\SURF']);

        [X_Opt_C,Y_Opt_C,ALL_O_C,TREE_O_C]=import_all_simu_FOLD([path '\410\opt\CYTO']);
        [X_Opt_S,Y_Opt_S,ALL_O_S,TREE_O_S]=import_all_simu_FOLD([path '\410\opt\SURF']);

        
        cd(here);
        save all_simu_data
        clear all
        
    end
    load('all_simu_data.mat');


end