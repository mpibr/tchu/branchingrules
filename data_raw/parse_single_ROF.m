function [ RES ] = parse_single_ROF( ROF, path_data_folder )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    path=[path_data_folder 'GFP\'];
    cd (path);
    FOL=1:13;
    OUTCOME_GFP=import_branches_22(ROF,FOL);
    cd ..

    path=[path_data_folder 'Ngl\'];
    cd (path);
    FOL=1:13;
    OUTCOME_NGL=import_branches_22(ROF,FOL);
    cd ..

%%

    assign_values;
    
    gfp=(v1_1_n+v2_1_n)./(v0_1_n);
    ngl=(v1_2_n+v2_2_n)./(v0_2_n);

    N_resampling=100;

    RR.n_ngl_vec=ngl;
    RR.n_gfp_vec=gfp;

    RR.GFP_0= v0_1;
    RR.GFP_1= v1_1;
    RR.GFP_2= v2_1;
    RR.GFP_0BG= v0_1_BG;
    RR.GFP_1BG= v1_1_BG;
    RR.GFP_2BG= v2_1_BG;


    RR.NGL_0= v0_2;
    RR.NGL_1= v1_2;
    RR.NGL_2= v2_2;
    RR.NGL_0BG= v0_2_BG;
    RR.NGL_1BG= v1_2_BG;
    RR.NGL_2BG= v2_2_BG;


    RR.GFP_indx=OUTCOME_GFP.indx;
    RR.NGL_indx=OUTCOME_NGL.indx;

    RR.GFP_N_fig=OUTCOME_GFP.N_fig;
    RR.NGL_N_fig=OUTCOME_NGL.N_fig;




    RES.ngl=zeros(length(RR.NGL_0),4);
    RES.ngl_BG=zeros(length(RR.NGL_0),4);
    RES.gfp=zeros(length(RR.GFP_0),4);
    RES.gfp_BG=zeros(length(RR.GFP_0),4);

    
    RES.ngl(:,1)=RR.NGL_0;
    RES.ngl(:,2)=RR.NGL_1;
    RES.ngl(:,3)=RR.NGL_2;
    RES.ngl(:,4)=(RES.ngl(:,2)+RES.ngl(:,3))./RES.ngl(:,1);

    
    RES.ngl_BG(:,1)=RR.NGL_0BG;
    RES.ngl_BG(:,2)=RR.NGL_1BG;
    RES.ngl_BG(:,3)=RR.NGL_2BG;
    RES.ngl_BG(:,4)=(RES.ngl_BG(:,2)+RES.ngl_BG(:,3))./RES.ngl_BG(:,1);
    
    
    RES.ngl_N(:,1)=RR.NGL_0-RR.NGL_0BG;
    RES.ngl_N(:,2)=RR.NGL_1-RR.NGL_1BG;
    RES.ngl_N(:,3)=RR.NGL_2-RR.NGL_2BG;
    RES.ngl_N(:,4)=(RES.ngl_N(:,2)+RES.ngl_N(:,3))./RES.ngl_N(:,1);


    RES.gfp(:,1)=RR.GFP_0;
    RES.gfp(:,2)=RR.GFP_1;
    RES.gfp(:,3)=RR.GFP_2;
    RES.gfp(:,4)=(RES.gfp(:,2)+RES.gfp(:,3))./RES.gfp(:,1);


    RES.gfp_BG(:,1)=RR.GFP_0BG;
    RES.gfp_BG(:,2)=RR.GFP_1BG;
    RES.gfp_BG(:,3)=RR.GFP_2BG;
    RES.gfp_BG(:,4)=(RES.gfp_BG(:,2)+RES.gfp_BG(:,3))./RES.gfp_BG(:,1);


    RES.gfp_N(:,1)=RR.GFP_0-RR.GFP_0BG;
    RES.gfp_N(:,2)=RR.GFP_1-RR.GFP_1BG;
    RES.gfp_N(:,3)=RR.GFP_2-RR.GFP_2BG;
    RES.gfp_N(:,4)=(RES.gfp_N(:,2)+RES.gfp_N(:,3))./RES.gfp_N(:,1);

    
    RES.GFP_indx=RR.GFP_indx;
    RES.NLG_indx=RR.NGL_indx;
    RES.GFP_FIG =RR.GFP_N_fig;
    RES.NLG_FIG =RR.NGL_N_fig;
    
    
    
    RES.m=0;
    RES.D=0;



end

