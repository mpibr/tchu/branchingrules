function [R0,R1,R2] = lengths( path )
    here=pwd;
    cd(path);
    R0=[];
    R1=[];
    R2=[];
    
    cd('.\Ngl')
	count=1;
    
    for i=1:13
        [i count]
        cd(['.\' num2str(i)])
        cd('.\lengths')
        L=length(dir('*.txt'));

        N=L/12;
        if(mod(L,12)==0)
            for j=1:L
                A=importdata([num2str(j-1) '.txt']);
                rad(j)=A.data(end);
            end
            for j=1:N
                R0(count)=rad(1+((j-1)*12));
                R0(count)=rad(2+((j-1)*12))+R0(count);
                R0(count)=rad(3+((j-1)*12))+R0(count);
                R0(count)=rad(4+((j-1)*12))+R0(count);
                
                R1(count)=rad(5+((j-1)*12));
                R1(count)=rad(6+((j-1)*12))+R1(count);
                R1(count)=rad(7+((j-1)*12))+R1(count);
                R1(count)=rad(8+((j-1)*12))+R1(count);

                R2(count)=rad(9+((j-1)*12));
                R2(count)=rad(10+((j-1)*12))+R2(count);
                R2(count)=rad(11+((j-1)*12))+R2(count);
                R2(count)=rad(12+((j-1)*12))+R2(count);

                count=count+1;
            end
        else
            disp('problem')
            pause()
        end
        cd ./..
        cd ..
    end
    cd ..

    cd('.\GFP')
    for i=1:13
                [i count]
        cd(['.\' num2str(i)])
        cd('.\lengths')
        L=length(dir('*.txt'));

        N=L/12;
        if(mod(L,12)==0)
            for j=1:L
                A=importdata([num2str(j-1) '.txt']);
                rad(j)=A.data(end);
            end
            for j=1:N
                R0(count)=rad(1+((j-1)*12));
                R0(count)=rad(2+((j-1)*12))+R0(count);
                R0(count)=rad(3+((j-1)*12))+R0(count);
                R0(count)=rad(4+((j-1)*12))+R0(count);

                R1(count)=rad(5+((j-1)*12));
                R1(count)=rad(6+((j-1)*12))+R1(count);
                R1(count)=rad(7+((j-1)*12))+R1(count);
                R1(count)=rad(8+((j-1)*12))+R1(count);

                R2(count)=rad(9+((j-1)*12));
                R2(count)=rad(10+((j-1)*12))+R2(count);
                R2(count)=rad(11+((j-1)*12))+R2(count);
                R2(count)=rad(12+((j-1)*12))+R2(count);

                count=count+1;
            end
        else
            disp('problem')
            pause()
        end
        cd ./..


        cd ..
    end
    cd ..

    R0=R0./4;
    R1=R1./4;
    R2=R2./4;

    cd(here)
end

