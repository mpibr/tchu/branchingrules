function [X,Y,ALL,TREE] = import_all_simu_FOLD(path)
    here=pwd;
    addpath(here);
    
    cd(path);

	dx=8;
    Xmax=2000;
    X=0:dx:Xmax;
    L=length(X);
    Y=zeros(1,L);
    
    
    NAMES=dir('Case_*');
    LG=length(NAMES);
    ALL=[];
    for j=1:LG
        j
        cd(['./' NAMES(j).name])
            NL=length(dir('DEND*'));
            A=importdata(['DEND=' num2str(NL-1) '.txt']);
            ALL=[ALL;A(:,end)];
            [Xt,Yt,Nt] = binning_2(A,dx,Xmax+2);
            Y=Y+Yt;
            if((strcmp(NAMES(j).name,'case_spare.CNG.swc')))
                TREE=A;
            end

        cd ..
    end
    
    



    rmpath(here);
    cd(here);
end

