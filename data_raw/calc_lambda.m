function [ Ls,Lc ] = calc_lambda(r1,r2,L1,L2,x)
    
    count=1;
    for i=1:length(r1)
        if(min(L1(i),L2(i))>0)
            if(max(L1(i),L2(i))<9999999)
                if(L1(i)<L2(i))
                    if(r1(i)<r2(i))
                        K=cosh(L1(i)./x)./cosh(L2(i)./x);
                        R=r1(i)./r2(i);
                        ERR=abs(R-K);
                        indx=find(ERR==min(ERR));
                        Ls(count)=mean(x(indx));

                        ERR=abs(R^2-K);
                        indx=find(ERR==min(ERR));
                        Lc(count)=mean(x(indx));
                        count=count+1;
                    end
                end
                if(L1(i)>L2(i))
                    if(r1(i)>r2(i))
                        K=cosh(L1(i)./x)./cosh(L2(i)./x);
                        R=r1(i)./r2(i);
                        ERR=abs(R-K);
                        indx=find(ERR==min(ERR));
                        Ls(count)=mean(x(indx));

                        ERR=abs(R^2-K);
                        indx=find(ERR==min(ERR));
                        Lc(count)=mean(x(indx));                    
                        count=count+1;

                    end
                end
                
                
            end
        end
    end





end

