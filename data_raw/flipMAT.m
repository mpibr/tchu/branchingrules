function [ RES ] = flipMAT( MAT )
    RES=MAT;

    for i=1:size(MAT,1)
        RES(i,:)=MAT(end-i+1,:);
    end
    
    ML=max(RES(:,1));
    
    for i=1:size(MAT,1)
        RES(i,1)=ML-RES(i,1);
    end
    

end

