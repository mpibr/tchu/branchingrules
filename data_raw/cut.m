function [RES] = cut( MAT, ROF )


    x=MAT(:,1);
    y=MAT(:,2);

    m=ROF(1);
    M=ROF(2);

    
    if(length(find(x==m))==1)
        INDX_m=find(x==m);
    else
        INDX_m=[max(find(x<m)),max(find(x<m))+1];
    end
    
    if(length(find(x==M))==1)
        INDX_M=find(x==M);
    else
        INDX_M=[min(find(x>M))-1,min(find(x>M))];
    end

    
    
    
    
    if(max(INDX_m)<1+min(INDX_M))
        complete=INDX_m+1:INDX_M+1;
        RES=MAT(complete,:);
    else
        RES=[];
    end
    
    
    if(length(INDX_m)==1)
        RES=[MAT(INDX_m,:);RES];
    else
         XX_mm=m;
         alpha=(m-x(INDX_m(1)))/abs(INDX_m(2)-INDX_m(1));         
         YY_mm=y(INDX_m(2))+alpha*y(INDX_m(2));
         RES=[XX_mm,YY_mm;RES];
    end
    
    
	if(length(INDX_M)==1)
        RES=[RES;MAT(INDX_M,:)];
    else
        XX_MM=M;
        alpha=(m-x(INDX_M(1)))/abs(INDX_M(2)-INDX_M(1));
        YY_MM=y(INDX_M(2))+alpha*y(INDX_M(2));
        RES=[RES;XX_MM,YY_MM];

    end
    
    
    

end

