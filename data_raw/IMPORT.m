function IMPORT(path)

    if(exist('all_bio_data.mat')==2)

    else
        if(exist(path)==0)
            path='C:\Users\sartorif\Desktop\Fluorescence';
        end
        OTO=load_radii_OTO();
        ALI=PYR_NEURON();

        [protein_lambdas]=diff_lengths();


        [ALI.Ls,ALI.Lc]=calc_lambda(ALI.R1,ALI.R2,ALI.L1,ALI.L2,0:0.05:6000);



        [R0_C,R1_C,R2_C]=Cultured_neuron(path);


        save all_bio_data
        clear all
    end
    load('all_bio_data.mat');


end