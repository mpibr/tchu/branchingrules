function [X,Y,N] = binning_2(A,dx,Xmax)
Ln=length(0:dx:Xmax);

Y=zeros(1,Ln);
N=zeros(1,Ln);

NSOMA=0;

for i=1:length(A)
	indx=round(A(i,4)/dx)+1;
    if(indx>Ln)
        indx=Ln;
    end
    if(indx==0)
        indx=1;
    end
    Y(indx)=Y(indx)+A(i,end);
    N(indx)=N(indx)+1;    
    
 
end

X=0:dx:Xmax;

Y=Y./sum(Y);

Y=Y./(1-NSOMA);

end

